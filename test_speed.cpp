//  // INCLUDES -------------------------------------------------------------------
//  #include <stdlib.h>
//  #include <stdio.h>
//  #include <sys/time.h>
//  #include <time.h>

//  // DEFINES -------------------------------------------------------------------
//  // The original problem was here. The MAXDIM was 500. But we were using arrays
//  // that had a size of 512 in each dimension. This caused a buffer overrun that
//  // the dim variable and caused it to be reset to 0. The result of this was causing
//  // the multiplication loop to fall out before it had finished (as the loop was
//  // controlled by this global variable.
//  //
//  // Everything now uses the MAXDIM variable directly.
//  // This of course gives the C code an advantage as the compiler can optimize the
//  // loop explicitly for the fixed size arrays and thus unroll loops more efficiently.
//  #define MAXDIM 512
//  #define RUNS 100

//  // MATRIX FUNCTIONS ----------------------------------------------------------
//  class matrix
//  {
//  public:
//  matrix(int dim)
//        : dim_(dim)
//  {
//          data_ = new int[dim_ * dim_];

//  }

//      inline int dim() const {
//                          return dim_;
//                  }
//                  inline int& operator()(unsigned row, unsigned col) {
//                          return data_[dim_*row + col];
//                  }

//                  inline int operator()(unsigned row, unsigned col) const {
//                          return data_[dim_*row + col];
//                  }

//  private:
//      int dim_;
//      int* data_;
//  };

// // ---------------------------------------------------
// void random_matrix(int (&matrix)[MAXDIM][MAXDIM]) {
//          for (int r = 0; r < MAXDIM; r++)
//                  for (int c = 0; c < MAXDIM; c++)
//                          matrix[r][c] = rand() % 100;
// }
// void random_matrix_class(matrix& matrix) {
//          for (int r = 0; r < matrix.dim(); r++)
//                  for (int c = 0; c < matrix.dim(); c++)
//                          matrix(r, c) = rand() % 100;
//  }

// template<typename T, typename M>
//  float run(T f, M const& a, M const& b, M& c)
//  {
//          float time = 0;

//          for (int i = 0; i < RUNS; i++) {
//                  struct timeval start, end;
//                  gettimeofday(&start, NULL);
//                  f(a,b,c);
//                  gettimeofday(&end, NULL);

//                  long s = start.tv_sec * 1000 + start.tv_usec / 1000;
//                  long e = end.tv_sec * 1000 + end.tv_usec / 1000;

//                  time += e - s;
//          }
//          return time / RUNS;
//  }
//  // SEQ MULTIPLICATION ----------------------------------------------------------
//   void mult_seq(int const(&a)[MAXDIM][MAXDIM], int const(&b)[MAXDIM][MAXDIM], int (&z)[MAXDIM][MAXDIM]) {
//           for (int r = 0; r < MAXDIM; r++) {
//                   for (int c = 0; c < MAXDIM; c++) {
//                           z[r][c] = 0;
//                           for (int i = 0; i < MAXDIM; i++)
//                                   z[r][c] += a[r][i] * b[i][c];
//                   }
//           }
//   }
//   void mult_std(matrix const& a, matrix const& b, matrix& z) {
//           for (int r = 0; r < a.dim(); r++) {
//                   for (int c = 0; c < a.dim(); c++) {
//                           z(r,c) = 0;
//                           for (int i = 0; i < a.dim(); i++)
//                                   z(r,c) += a(r,i) * b(i,c);
//                   }
//           }
// 	  printf("%d ", z(0,0));
//   }

//   // MAIN ------------------------------------------------------------------------
// using namespace std;
// int main() {
//           srand(time(NULL));

//           int matrix_a[MAXDIM][MAXDIM];
//           int matrix_b[MAXDIM][MAXDIM];
//           int matrix_c[MAXDIM][MAXDIM];
//           random_matrix(matrix_a);
//           random_matrix(matrix_b);
//           printf("%d ", MAXDIM);
//           printf("%f \n", run(mult_seq, matrix_a, matrix_b, matrix_c));

//           matrix a(MAXDIM);
//           matrix b(MAXDIM);
//           matrix c(MAXDIM);
//           random_matrix_class(a);
//           random_matrix_class(b);
//           printf("%d ", MAXDIM);
//           //printf("%f \n", run(mult_std, a, b, c));

//           return 0;
//   }

#include <stdio.h> 
#include <stdlib.h> 
#include <time.h>

#define NUM 1024
float a[NUM][NUM],b[NUM][NUM],c[NUM][NUM]; 
void initialize_matrix(float m[][NUM]); 
void load_matrix(float m[][NUM]); 

int main() {

    int i,j,k; 

    clock_t t_inicial,t_final; 
    load_matrix(a); 
    load_matrix(b); 
    initialize_matrix(c); 

    printf("Starting matrix multiplication 1024x1024...\n\n");

    t_inicial=clock();
    for(i=0;i<NUM;i++) 
    for(j=0;j<NUM;j++) 
    for(k=0;k<NUM;k++) 
    c[i][j] =c[i][j] + a[i][k] * b[k][j]; 
    t_final=clock();
    printf("Matrix multiplication finished in: %3.6f seconds",((float) t_final- (float)t_inicial)/CLOCKS_PER_SEC);

} 

void initialize_matrix(float m[][NUM]) {

    int i,j;
    for(i=0;i<NUM;i++) 
    for(j=0;j<NUM;j++) 
    m[i][j]=0.0; 
    return;

} 
void load_matrix(float m[][NUM]) {

    int i,j;
   // #pragma omp parallel for
    for(i=0;i<NUM;i++) 
    for(j=0;j<NUM;j++) 
    m[i][j]=(float) 10*rand()/(float) rand(); 
    return;

}
