#include <boost/math/distributions.hpp>
#include <iostream>

void distributionFunc1(){
  boost::math::normal_distribution<> d(0.5, 1);

  std::cout << "CDF:" << cdf(d,0.2) << std::endl;
  std::cout << "Kurtosis:" << kurtosis(d) << std::endl;

  std::pair<double, double> sup = support(d);
  std::cout << "Left sup: "<< sup.first << std::endl;
  std::cout << "Right sup: "<< sup.second << std::endl;

}

int main(){
  distributionFunc1();
  return 0;
}

// #include <boost/lambda/lambda.hpp>
// #include <iostream>
// #include <iterator>
// #include <algorithm>

// int main()
// {
//   using namespace boost::lambda;
//   typedef std::istream_iterator<int> in;

//   std::for_each(
//                 in(std::cin), in(), std::cout << (_1 * 3) << " " );
// }
// #include <iostream>
// #include <iomanip>
// #include <string>
// #include <map>
// #include <random>
// #include <cmath>
// int main()
// {
//   std::random_device rd;
//   std::mt19937 gen(rd());
 
//   // values near the mean are the most likely
//   // standard deviation affects the dispersion of generated values from the mean
//   std::normal_distribution<> d(5,2);
 
//   std::map<int, int> hist;
//   for(int n=0; n<10000; ++n) {
//     ++hist[std::round(d(gen))];
//   }
//   for(auto p : hist) {
//     std::cout << std::fixed << std::setprecision(1) << std::setw(2)
//               << p.first << ' ' << std::string(p.second/200, '*') << '\n';
//   }
// }
