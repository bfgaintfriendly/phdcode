#!/bin/sh

python $HOME/Documents/phdcode/dynamics/perceptron.py 100.0 -1.5 100 4.5 1.5 100 frsb_unsat_1.5_alpha_100_100
python $HOME/Documents/phdcode/dynamics/perceptron.py 100.0 -1.5 100 4.5 1.5 500 frsb_unsat_1.5_alpha_100_500
python $HOME/Documents/phdcode/dynamics/perceptron.py 200.0 -1.5 100 4.5 1.5 100 frsb_unsat_1.5_alpha_200_100
python $HOME/Documents/phdcode/dynamics/perceptron.py 200.0 -1.5 100 4.5 1.5 500 frsb_unsat_1.5_alpha_200_500
