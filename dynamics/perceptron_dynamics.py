# imports 

import numpy as np 
import scipy 
import seaborn as sns 
import matplotlib.pyplot as plt
from scipy.integrate import quad 
from scipy.optimize import brentq, newton, ridder, bisect

def initialise(alpha, N,p_bar, Delta):
    M = int(alpha*N)
    return np.random.randn(int(M),N), np.random.normal(p_bar, np.sqrt(Delta),N)


def jamming_integrand(h,sigma, eta = 0.0):
    ''' Integrand which enters for the computation of the 
        jamming line'''
    den = 1.0/np.sqrt(2*np.pi*(1+eta))
    fun = np.exp(-(h*h)/(2*(1+eta)))
    fac = (h-sigma)**2
    return fun*fac*den

def standard_integrand(h, sigma, eta):
    ''' Integrand for the second integral entering 
        the computation for the UNSAT instability'''
    den = 1.0/np.sqrt(2*np.pi*(1+eta))
    fun = np.exp(-(h*h)/(2*(1+eta)))
    return den*fun

def func_to_solve_UNSAT(chi,m, sigma = 0.0):
    ''' Function to compute the value of '''
    return (1-m**2)*(quad(standard_integrand, -np.inf, chi, args = (chi, sigma))[0]) - quad(jamming_integrand, -np.inf,           chi, args = (chi,sigma))[0]

def compute_chi(m, sigma = 0.0):
    return newton(func_to_solve_UNSAT, 0.0, args = (m, sigma), maxiter = 500)

def compute_alpha_J(sigma, m, eta = 0.0):
    ''' Computes a particular value on the jamming line 
        for a given value of chi and m'''
    integral = quad(jamming_integrand, -np.inf, sigma, args = (sigma,eta))
    return (1-m**2)/integral[0]    
    
def jamming_line(sigma_range, m, eta = 0.0):
    ''' Computes the jamming line for a range of values of chi'''
    alpha = []
    for sigma in sigma_range:
        integral = quad(jamming_integrand, -np.inf, sigma, args = (sigma, eta))
        alpha.append((1-m**2)/integral[0])
    return np.asarray(alpha)

def compute_rhs(prices, sigma):
    h_mu = ((1.0/np.sqrt(N))*xi_matrix.dot(prices)) - sigma
    temp = np.multiply(h_mu, np.heaviside(-1.0*h_mu, 0.0))
    del_h = (1.0/np.sqrt(N))*temp.dot(xi_matrix)
    return del_h

def compute_energy(prices, sigma):
    h_mu = ((1.0/np.sqrt(N))*xi_matrix.dot(prices)) - sigma
    energy = 0.5*np.sum(np.square(np.multiply(h_mu, np.heaviside(-1.0*h_mu, 0.0))))
    energy = energy/N
    return energy


def loop(prices, sigma, budgets, m, Delta, max_iter=100000, step_size = 1e-3, tol = 1e-5, tol_energy = 1e-7):
    converge = False
    energy_list = []
    norm_list = []
    iteration_number = 0
    while (converge != True) and iteration_number < max_iter:
        del_x, energy = compute_rhs(prices, sigma)
        Y = prices - del_x*step_size
        Y[-1] = -1.0*np.sum(Y[:N-1])
        nu = np.sqrt(np.average(np.square(Y)))
        nu = nu/np.sqrt((1-m**2))
        Y = Y/nu
        diff = np.linalg.norm(Y - prices)
        temp_del_x, temp_energy = compute_rhs(Y, sigma)
        energy_1 = compute_energy(Y+m, sigma)
        energy_2 = compute_energy(prices+m, sigma)
        if np.abs(energy_1-energy_2) < tol_energy:
            prices = Y 
            converge = True
        else:
            converge = diff < tol
            prices = Y 
        iteration_number +=1
        del_norm = np.linalg.norm(del_x)
        if (iteration_number %1000 == 0.0):
            print "Iteration number: {}".format(iteration_number)
            print "{}  {} {} {} {} {} ".format(energy, diff, temp_energy, np.abs(temp_energy-energy), del_norm, energy_2)
        energy_list.append(energy)
        norm_list.append(del_norm)
    final_energy = compute_energy(prices+m, sigma)
    print "Final Energy: {}". format(final_energy)
    return energy_list, norm_list, prices, prices + np.ones(N)*m, final_energy
