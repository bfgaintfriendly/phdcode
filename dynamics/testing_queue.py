try:
    from queue import Queue, Empty
except ImportError:
    from Queue import Queue, Empty
from threading import Thread
import time
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


def make_data(queue):
    # Replace this with however your data comes in
    while True:
        queue.put([random.gauss(4, 1.5), random.gauss(6, 2.5)])
        time.sleep(0.1)


def update_line(t, queue, data, line):
    try:
        data.append(queue.get(block=False))
    except Empty:
        pass
    if data != []:
        line.set_data(np.array(data).T)
    return line,

fig1 = plt.figure()

q = Queue()
data = []
l, = plt.plot([], [], 'ro')
plt.xlim(-1, 11)
plt.ylim(-1, 11)
plt.xlabel('x')
plt.title('test')
line_ani = animation.FuncAnimation(fig1, update_line, 25, fargs=(q, data, l),
    interval=50, blit=True)

t = Thread(target=make_data, args=(q, ))
t.daemon = True
t.start()

plt.show()
