# Basic utility functions

# imports

import numpy as np
import scipy 
# import seaborn as sns 
import matplotlib.pyplot as plt
from scipy.integrate import quad 
from scipy.optimize import brentq, newton, ridder, bisect


# for calculating jamming stuff

def jamming_integrand(h,sigma, eta = 0.0):
    ''' Integrand which enters for the computation of the 
        jamming line'''
    den = 1.0/np.sqrt(2*np.pi*(1+eta))
    fun = np.exp(-(h*h)/(2*(1+eta)))
    fac = (h-sigma)**2
    return fun*fac*den

def standard_integrand(h, sigma, eta):
    ''' Integrand for the second integral entering 
        the computation for the UNSAT instability'''
    den = 1.0/np.sqrt(2*np.pi*(1+eta))
    fun = np.exp(-(h*h)/(2*(1+eta)))
    return den*fun
def func_to_solve_UNSAT(chi,m, sigma = 0.0):
    ''' Function to compute the value of '''
    return (1-m**2)*(quad(standard_integrand, -np.inf, chi, args = (chi, sigma))[0]) - quad(jamming_integrand, -np.inf, chi, args = (chi, sigma))[0]

def compute_chi(m, sigma = 0.0):
    return newton(func_to_solve_UNSAT, 0.0, args = (m, sigma), maxiter = 500)
#     return brentq(func_to_solve_UNSAT, -4.0, 3.0, args = (m, sigma))

def compute_alpha_J(sigma, m, eta = 0.0):
    ''' Computes a particular value on the jamming line 
        for a given value of chi and m'''
    integral = quad(jamming_integrand, -np.inf, sigma, args = (sigma,eta))
    return (1-m**2)/integral[0]    
    
def jamming_line(sigma_range, m, eta = 0.0):
    ''' Computes the jamming line for a range of values of chi'''
    alpha = []
    for sigma in sigma_range:
        integral = quad(jamming_integrand, -np.inf, sigma, args = (sigma, eta))
        alpha.append((1-m**2)/integral[0])
    return np.asarray(alpha)

def compute_energy_analytic(alpha, sigma, m):
    '''Computes the analytic energy'''
    alpha_J = compute_alpha_J(sigma, m)
    energy_analytic = 0.50*(1-m**2)*(np.sqrt(alpha/alpha_J) - 1)**2
    return energy_analytic

def compute_percentage_difference(a,b):
    return ((a*1.0-b)/a)*100
