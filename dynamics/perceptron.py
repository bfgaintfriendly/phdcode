from utils import *
import simplejson 
from numba import jit
from numba import float64
import sys 

@jit()
def _compute_gap(N, xi_matrix, prices, sigma, budgets):
    return ((1.0/np.sqrt(N))*xi_matrix.dot(prices)) - (sigma+budgets)
class Perceptron:

    def __init__(self, alpha, N, sigma, pbar, Delta,eta=0.0):
        self.alpha = alpha
        self.N = N
        self.sigma = sigma
        self.pbar = pbar
        self.Delta = Delta
        self.eta = eta
        self.M = int(alpha*N)

        self.m = self.pbar/np.sqrt(self.pbar**2 + self.Delta)

        ## set seeds for the xi_matrix and prices

        # self.seed_xi = 456761   
        # self.seed_prices = 897512
        # self.seed_budgets = 6432436

        self.init_xi()
        self.init_prices()
        self.init_budgets()

    def get_info(self):
        print "Alpha: {}".format(self.alpha)
        print "M: {}".format(self.M)
        print "N: {}".format(self.N)
        print "m: {}".format(self.m)
        
    def get_alpha(self):
        return self.alpha

    def init_xi(self):
        # np.random.seed(self.seed_xi) 
        self.xi_matrix = np.random.randn(self.M, self.N)
        
    def init_prices(self):
       #  np.random.seed(self.seed_prices)
        self.prices = np.random.normal(self.m, 1.0, self.N)

    def init_budgets(self):
        # np.random.seed(self.seed_budgets)
        if self.eta == 0.0:
            self.budgets = 0.0
        else:
            self.budgets = np.random.normal(0, self.eta)

    def compute_gap(self):
        
        return _compute_gap(self.N, self.xi_matrix, self.prices, self.sigma, self.budgets)
    
    def compute_energy(self):
        h_mu = self.compute_gap()
        # energy = 0.5*np.sum(np.square(np.multiply(h_mu, np.heaviside(-1.0*h_mu, 0.0))))
        energy = 0.5*np.sum(np.square(np.multiply(h_mu, h_mu<0.0)))
        energy = energy/self.N
        return energy
    
    def compute_gradient(self):
        h_mu = self.compute_gap()
        # temp = np.multiply(h_mu, np.heaviside(-1.0*h_mu, 0.0))
        temp = np.multiply(h_mu, h_mu <0.0)
        del_h = (1.0/np.sqrt(self.N))*temp.dot(self.xi_matrix)
        return del_h

    def set_gap(self):
        self.h_mu = self.compute_gap()
        
    def compute_violated_constraints(self):
        self.set_gap()
        self.viocons = self.h_mu <  0.0
        self.c = np.count_nonzero(self.viocons)
        self.z = self.c/(1.0*self.M)
        return self.c

    def replace_violated_constraints(self):
        if self.c != 0:
            replacements = np.random.randn(self.c, self.N)
            self.xi_matrix[self.viocons, ::] = replacements

    def get_energy_analytic(self):
        return compute_energy_analytic(self.alpha, self.sigma, self.m)

    def sat_or_unsat(self):
        ''' Returns true if perceptron in SAT phase or UNSAT phase '''
        if self.alpha > compute_alpha_J(self.sigma, self.m, self.eta):
            self.state = 'UNSAT'
            return False
        else:
            self.state = 'SAT'
            return True

    def stats_prices(self):
        self.p_average = np.average(self.prices)
        self.p_squared = np.average(np.square(self.prices))
        return self.p_average, self.p_squared

    def jamming_limit(self):
        return compute_alpha_J(self.sigma, self.m, self.eta)   
        
    def run_gd(self, step_size = 1e-4, max_iter = 20000, period = 1000, verbose=False):
        self.energy_list = []
        self.norm_list = []
        self.energy_list.append(self.compute_energy())
        i=0
        iteration_number = 0
        lambda_constraint = 50000
        mu_constraint = 50000
        del_norm = 1000000
        energy_analytic = compute_energy_analytic(self.alpha, self.sigma,self.m)
        print "Iteration_number:{} Lambda:{} Mu: {}".format(i+1, lambda_constraint, mu_constraint)
        while (del_norm > 1e-6) and (lambda_constraint <= 1e7 and mu_constraint <= 1e7) and (iteration_number <200000):
            
            del_x = self.compute_gradient()
            complete_gradient = del_x + lambda_constraint*(np.average(np.square(self.prices)) - 1)*(2*self.prices/self.N) + (mu_constraint/self.N)*(np.average(self.prices)-self.m)
            Y = self.prices - complete_gradient*step_size
            del_norm = np.linalg.norm(complete_gradient)
            self.prices = Y
            energy_2 = self.compute_energy()
            diff = energy_2 - self.energy_list[-1]
            iteration_number +=1
            p_average = np.average(self.prices)
            p_squared = np.average(np.square(self.prices))
            if (verbose):
                if (iteration_number %period== 0.0):
                    print "Iteration number: {}".format(iteration_number)
                    print "{} {} {} {} {}".format(energy_2, diff, del_norm,p_average, p_squared)
            self.energy_list.append(energy_2)
            self.norm_list.append(del_norm)
            if (iteration_number%max_iter == 0.0):
                i+=1 
                print "Energy: {}, Average Prices: {}, Prices Squared: {} Gradient Norm: {}". format(energy_2, p_average, p_squared, del_norm)
                print "Percentage Difference : {}".format(compute_percentage_difference(energy_2, energy_analytic))
                lambda_constraint = lambda_constraint*1.1
                mu_constraint = mu_constraint*1.1
                print "\n\n"
                print "Iteration_number:{} Lambda:{} Mu: {}".format(i+1, lambda_constraint, mu_constraint)
                
                
        print "Loop exited \n"
        print "Final Energy: {}, Average Prices: {}, Prices Squared: {} Gradient Norm: {}". format(energy_2, p_average, p_squared, del_norm)
        print "Analytic value of energy: {} Percentage Difference : {}".format(energy_analytic, compute_percentage_difference(energy_2, energy_analytic))
        return iteration_number
    
if __name__ == "__main__":
    alpha = float(sys.argv[1])
    sigma = float(sys.argv[2])
    N = int(sys.argv[3])
    Delta = float(sys.argv[4])
    pbar = float(sys.argv[5])
    T = int(sys.argv[6])

    test = Perceptron(alpha, N, sigma, pbar, Delta)
    energies = []
    zs = []
    iterations = []
    
    for i in range(T):
        print "Trial number: {}".format(i+1)
        print "No. of violated constraints (before):{} ".format(test.compute_violated_constraints())
        zs.append(test.z)
        print "Running GD"
        iter_number = test.run_gd()
        iterations.append(iter_number)
        energies.append(test.energy_list[-1])
        print "Replacing constraints"
        test.replace_violated_constraints()
        print "No. of unviolated constraints (after):{} ".format(test.compute_violated_constraints())
    
    energies = np.asarray(energies)
    zs = np.asarray(zs)
    print "Writing data to file"
    data_file = sys.argv[7]+".dat"
    file_id = open(data_file, "w+")
    data_array = np.array([range(1,T+1), energies, zs,np.asarray(iterations)])
    data_array = data_array.T
    np.savetxt(file_id, data_array)
    file_id.close()
    with open(sys.argv[7]+"params.dat", "w+") as f:
        simplejson.dump(sys.argv[1:7], f)
    
    
    
