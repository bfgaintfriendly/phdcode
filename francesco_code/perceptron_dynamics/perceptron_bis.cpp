#include<iostream>
#include"casuale.h"
#include<time.h>

using namespace std;


int main() {

  /* system initial parameters */
  int N=10;
  double alpha=5.0;   //density of constraints
  double T=0.0;   //temperature
  double sigma=10;    //stress
  double epsilon=1;  //energy scale
  double h0=0;      //this is the constraints parameter
  double dt=0.001;  // integration time step

  /* run parameters */
  double tfin=1000;
  double tprint=0.1;
  double teq=300;
  //  long seed=-time(NULL);
  long seed=-4378654;

 /* other constants */
  double sqN=sqrt(N);
  double namp=sqrt(2*T*dt);
  int M=int(N*alpha);
    
  /* initialize the disorder */
  double J[N][N];
  double xi[N][M];
  for (int i=0;i<N;i++) {
    J[i][i]=0;
    for (int j=i+1;j<N;j++) {
      J[i][j]=gasdev(&seed)/sqN;
      J[j][i]=-J[i][j];
    }
    for (int mu=0;mu<M;mu++) {
      xi[i][mu]=gasdev(&seed)/sqN;
    }    
  }
  
  // We set the J_{i}}{j} = 0
  
  for (int i=0; i< N; i++){
      for (int j=0; i<N; j++){
          J[i][j] = 0;
      }
  }


  /* initialize X */  
  double X[N];
  double Xnorm=0;
  for (int i=0;i<N;i++) {
    X[i]=( ran1(&seed)>0.5 ? 1 : -1 );
    Xnorm+=X[i]*X[i];
  }
  Xnorm=sqrt(Xnorm);
  for (int i=0;i<N;i++) {
    X[i]*=(sqN/Xnorm);
  }

  int nrun=20;
  for (int r=0;r<nrun;r++) {

    /* change parameters at the beginning of the run */
    if (r>0) sigma-=0.5;
    /* end */
 
    /* time loop */
    double h[M];
    double Fnc[N];
    double Fc[N];
    double Y[N];
    double em=0;
    double pm=0;
    double zm=0;
    double Pcm=0;
    double Pncm=0;
    double counter=0;
    for (int t=0;t<int(tfin/dt);t++) {
      /* compute the hmu */
      for (int mu=0;mu<M;mu++) {
	h[mu]=0;
	for (int i=0;i<N;i++) {
	  h[mu]+=X[i]*xi[i][mu];
	}
	h[mu]-=h0;
      }
      /* compute the non-conservative force */      
      for (int i=0;i<N;i++) {
	Fnc[i]=0;
	Fc[i]=0;
	for (int j=0;j<N;j++) {
	  Fnc[i]+=J[i][j]*X[j];
	}
      }
      /* compute the conservative force */
      for (int mu=0;mu<M;mu++) {
	if (h[mu]<0) {
	  for (int i=0;i<N;i++) {
	    Fc[i]-=h[mu]*xi[i][mu];
	  }
	}
      }    
      /* compute the Y and update the X */
      double nu=0;
      for (int i=0;i<N;i++) {
	Y[i]=X[i]+(epsilon*Fc[i]+sigma*Fnc[i])*dt+namp*gasdev(&seed);
	nu+=Y[i]*Y[i];
      }
      nu=sqrt(nu/N);
      for (int i=0;i<N;i++) {
	X[i]=Y[i]/nu;
      }
      /* output */
      double e=0;
      double p=0;
      double z=0;
      for (int mu=0;mu<M;mu++) {
	if (h[mu]<-0.0000001) {
	  e+=h[mu]*h[mu]/2.;
	  p-=h[mu];
	  z++;
	}
      }
      e/=N;
      p/=N;
      z/=N;
      double Pc=0;
      double Pnc=0;
      for (int i=0;i<N;i++) {
	Pnc+=sigma*Fnc[i]*sigma*Fnc[i];
	Pc+=sigma*Fnc[i]*epsilon*Fc[i];
      }
      Pc/=N;
      Pnc/=N;
      if (t%(int(tprint/dt))==0) cout<<"t"<<r<<"t "<<t*dt<<" "<<z<<" "<<p<<" "<<e<<" "<<Pc<<" "<<Pnc<<endl;
      if (t*dt>teq) {
	em+=e;
	pm+=p;
	zm+=z;
	Pcm+=Pc;
	Pncm+=Pnc;
	counter++;
      }
    }
    cout<<"moy "<<alpha<<" "<<h0<<" "<<sigma<<" "<<zm/counter<<" "<<pm/counter<<" "<<em/counter<<" "<<Pcm/counter<<" "<<Pncm/counter<<endl;
    
  }
    
  return 0;

}
