T=$1
sigma=-0.5
echo "Running with sigma = $sigma"
for i in 1 2 3 4 5
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 500 $T
    command; echo "Alpha = $i done. Sigma = $sigma" | mail -s "Progress alpha = $i" sharma@lpt.ens.fr
done
command; echo "Small alpha till 5 done. Will run large alpha now" | mail -s "Simulation update - 1" sharma@lpt.ens.fr
for i in 10 20 30 40 50 60 70 80 90 100
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 100 $T
    command; echo "Alpha = $i done. Sigma = $sigma" | mail -s "Progress alpha = $i" sharma@lpt.ens.fr
done

sigma=-1.0
echo "Running with sigma = $sigma"
for i in 1 2 3 4 5
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 500 $T
    command; echo "Alpha = $i done. Sigma = $sigma" | mail -s "Progress alpha = $i" sharma@lpt.ens.fr
done
command; echo "Small alpha till 5 done. Will run large alpha now" | mail -s "Simulation update - 1" sharma@lpt.ens.fr
for i in 10 20 30 40 50 60 70 80 90 100
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 100 $T
    command; echo "Alpha = $i done. Sigma = $sigma" | mail -s "Progress alpha = $i" sharma@lpt.ens.fr
done

sigma=-1.5
echo "Running with sigma = $sigma"
for i in 1 2 3 4 5
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 500 $T
    command; echo "Alpha = $i done. Sigma = $sigma" | mail -s "Progress alpha = $i" sharma@lpt.ens.fr
done
command; echo "Small alpha till 5 done. Will run large alpha now" | mail -s "Simulation update - 1" sharma@lpt.ens.fr
for i in 10 20 30 40 50 60 70 80 90 100
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 100 $T
    command; echo "Alpha = $i done. Sigma = $sigma" | mail -s "Progress alpha = $i" sharma@lpt.ens.fr
done

sigma=-2.0
echo "Running with sigma = $sigma"
for i in 1 2 3 4 5
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 500 $T
    command; echo "Alpha = $i done. Sigma = $sigma" | mail -s "Progress alpha = $i" sharma@lpt.ens.fr
done
command; echo "Small alpha till 5 done. Will run large alpha now" | mail -s "Simulation update - 1" sharma@lpt.ens.fr
for i in 10 20 30 40 50 60 70 80 90 100
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 100 $T
    command; echo "Alpha = $i done. Sigma = $sigma" | mail -s "Progress alpha = $i" sharma@lpt.ens.fr
done

