#include "perceptron.hpp"


// alpha,N, M sigma, Delta, pbar
int main(int argc, char *argv[]){
  double alpha = atof(argv[1]);
  double sigma = atof(argv[2]);
  int N = 0;
  if (argc > 3){
     N = atoi(argv[3]);
  }
  else{
    N = 100;
  }
  // double sigma = atof(argv[3]);
  // double Delta = atof(argv[4]);
  // double pbar = atof(argv[5]);
  // double eta = atof(argv[6]);
  // int maxiter = atoi(argv[7]);
  // double alpha = atof(argv[1]);
  
  //double alpha = 25.0;
  // int T = atoi(argv[3]);
  //int N = 100;
  //double sigma = 0.5;
  double Delta = 4.5;
  double pbar = 1.5;
  double eta = 0.0;
  
    
   Perceptron test(alpha, N, sigma, Delta, pbar, eta);
   std::cout << "Alpha = " << test.getalpha() << std::endl;
   std::cout << "M = " << test.getM() << std::endl;
   std::cout << "m = " << test.getm() << std::endl;
   std::cout << "sigma = " << test.getsigma() << std::endl;
   std::cout << "N = " << test.getN() << std::endl;

  // std::cout << "Testing repeal and replace "<< std::endl;
    int maxiter = 20000;
  
  // // std::cout<< test.viocons << std::endl;
  // // test.compute_gap();
  // // std::cout << "h_mu = " <<test.h_mu.transpose()<< std::endl;
  // // std::cout << "viocons = " << test.viocons.transpose() << std::endl;
  // // std::cout << "Z = " << test.z << std::endl;
  // // std::cout << "Compute constraints " << std::endl; 
  // // test.compute_violated_constraints();
  // // std::cout << "Z = " << test.z << std::endl;
  // // std::cout << "Viocons = " << test.viocons.transpose() << std::endl;
  // // std::cout << "Now replacing constraints " << std::endl;
  // // std::cout << "Run gd " << std::endl;
    //test.run_gd(maxiter);
  // // // test.compute_violated_constraints();
  // // std::cout << test.xi_matrix << std::endl;
  // // test.replace_violated_constraints();
  // // std::cout << "\n\n " << test.xi_matrix << std::endl;
  // // std::cout << "Now computing gaps" << std::endl;
  // // test.compute_gap();
  // // std::cout << "h_mu = " <<test.h_mu.transpose()<< std::endl;
  // // std::cout << "Viocons = " << test.viocons.transpose() << std::endl;
  // // std::cout << "Z = " << test.z << std::endl;
  // // test.compute_violated_constraints();
  // // std::cout << "Viocons = " << test.viocons.transpose() << std::endl;
  // // std::cout << "Z = " << test.z << std::endl;
  // std::cout << "-----------------------------------" << std::endl;
  // Vector energy_values(T);
  // energy_values.setZero();
  // Vector z_values(T);
  // z_values.setZero();
  // for (int i = 0; i < T ; i++){
  //   std::cout << "Running iteration number: " << i+1 << std::endl;
  //   test.run_gd(maxiter, 1000,true);
  //   std::cout << "Z = " << test.z << "Energy = " << test.energy << std::endl;
  //   energy_values(i) = test.energy;
  //   z_values(i) = test.z;
  //   // std::cout << "h_mu " << test.h_mu.transpose() << std::endl;
  //   std::cout<< "Replacing constraints " << std::endl;
  //   test.replace_violated_constraints();
  //   std::cout << "Z (after replacements) : " << test.z << std::endl;
   
  // }

  // std::cout << "Write stuff to file" << std::endl;

  // std::string filename = test.replacement_filename;

  // std::ofstream write_output(filename);
  // assert(write_output.is_open());

  // for (int i = 0; i < T; i++){
  //   write_output << i+1 << "\t" << z_values(i) << "\t" << energy_values(i) << std::endl;
  // }

  // write_output.close();
  // std::cout << "Data written to " << filename << std::endl;
    
    
}
