#include "perceptron.hpp"

int main(int argc, char *argv[]){

  Eigen::VectorXi N_values(8);
  N_values <<  10, 20, 50, 100, 200, 500, 1000, 2000;
  std::cout << "N vector" << N_values << std::endl;
  double alpha = 25.0;
  double sigma = 1.5;
  double Delta = 4.5;
  double pbar = 1.5;
  double eta = 0.0;;
  int maxiter = 5000;
  Vector energy_values(8);
  energy_values.setZero();

  for (int i = 0; i < N_values.size(); i++){
    std::cout << "----------------------------" << std::endl;
    std::cout << "N value " << N_values(i) << std::endl;
    Perceptron test(alpha, N_values(i), sigma, Delta, pbar, eta);

    test.run_gd(maxiter);
    int len_iterations = test.energy_list.size();
    energy_values(i) = test.energy_list(len_iterations-1);
  }

  std::cout << energy_values.transpose() << std::endl;

  std::string filename = "scaling.dat";

  std::ofstream write_output(filename);
  assert(write_output.is_open());

  for (int i = 0; i < N_values.size(); i++){
    write_output << N_values(i) << "\t" << energy_values(i) << std::endl;
  }

  write_output.close();
  std::cout << "Data written to " << filename << std::endl;
  
    
    
}
