#include <iostream>
// #include <Eigen/Dense>
#include <functional>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>
#include <typeinfo>
#include <Eigen/Eigenvalues> 
#include "perceptron.hpp"

bool is_file_exist(std::string filename){
  std::ifstream infile(filename);
  return infile.good();
}

Matrix normalize_cols(Perceptron & test){
  int N = test.getN();
  int M = test.getM();

  Matrix copy = test.xi_matrix;
  for (int i = 0; i < N; i++){
    double mean = test.xi_matrix.col(i).sum();
    mean = mean/M;
    double var = test.xi_matrix.col(i).squaredNorm();
    var = var/M;
    var = var - (mean*mean);
    Vector temp =  mean*(Vector::Constant(M,1.0));
    //std::cout << i <<  "\t" << temp.transpose() << std::endl;
    //std::cout << copy.col(i).cols() << "\t" << copy.col(i).cols() << std::endl;
    //std::cout << "Temp vector " << temp.cols() << "\t" << temp.cols() << std::endl;
    copy.col(i) = test.xi_matrix.col(i)-temp;
    //copy.col(i) = test.xi_matrix.col(i) - mean*(Vector::Constant(N,1.0));
    copy.col(i) = copy.col(i)/sqrt(var);
    //std::cout << "Sum of col test " << mean << " Var " << var << std::endl;
    // std::cout << "Sum of col copy " << copy.col(i).sum()/M << " Variance " << copy.col(i).squaredNorm()/M<<  std::endl;
  }

  return copy;
    
}



void rr_procedure(double alpha, int N, double sigma, double Delta, double pbar, int T, int copy, double eta=0.0){

 double eps = 1e-9;
 int maxiter = 20000;
 std::string underscore = std::string("_");
 std::string sigma_float = std::to_string(sigma);
 sigma_float.erase( sigma_float.find_last_not_of('0') + 1, std::string::npos);

 std::string alpha_float = std::to_string(alpha);
 alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

 std::string f3 = "prices";
 std::string f4 = "xi_matrix"; 
  
 std::string to_append = underscore + sigma_float + underscore + alpha_float + underscore + std::to_string(N) + underscore;

 Perceptron test(alpha, N, sigma, Delta, pbar, eta);
 std::ofstream rar_output(test.replacement_filename+to_append+std::to_string(copy));
 std::ofstream evals1_output(test.evals1_filename+to_append+std::to_string(copy));
 std::ofstream evals2_output(test.evals2_filename+to_append+std::to_string(copy));
 std::ofstream prices_output(f3 + to_append+std::to_string(copy));
 std::ofstream xi_output(f4 + to_append + std::to_string(copy));
 
    //std::ofstream overlap_output(f3+std::to_string(i));
    //assert(overlap_output.is_open());
 assert(rar_output.is_open());
 assert(evals1_output.is_open());
 assert(evals2_output.is_open());
 assert(prices_output.is_open());
 assert(xi_output.is_open());
 test.compute_evals();
 evals1_output << test.evals1.real().transpose() << std::endl;
 evals2_output << test.evals2.real().transpose() << std::endl;
 xi_output << test.xi_matrix << std::endl;

 //First pass to get energy
 test.run_gd(maxiter);
 // We save the price vector 
 prices_output << test.prices.transpose() << std::endl;
 // And now the stats
 rar_output << 0 << "\t" << test.z << "\t" << test.z*1.0/test.getM() << "\t" << test.energy << std::endl;
 int j = 1;
 // Now we use the remove and replace procedure
 while (j <=T) {
      std::cout << "Replacing agents" << std::endl;
      test.replace_violated_constraints();
      //Re-run the GD
      test.run_gd(maxiter);
      rar_output << j << "\t" << test.z << "\t" << test.z*1.0/test.getM() << "\t" << test.energy << std::endl;
      prices_output << test.prices.transpose() << std::endl;
      if (test.energy < eps){
        std::cout << "Energy reached zero. Stopping R&R procedure " << std::endl;
        break;
      }

      if (j%50 == 0 && j >0){
        test.compute_evals();
        evals1_output << test.evals1.real().transpose() << std::endl;
        evals2_output << test.evals2.real().transpose() << std::endl;
        xi_output << test.xi_matrix << std::endl;
      }
 }
 if (j != T){
    test.compute_evals();
    evals1_output << test.evals1.real().transpose() << std::endl;
    evals2_output << test.evals2.real().transpose() << std::endl;
    xi_output << test.xi_matrix << std::endl;
 }
 xi_output.close();
 evals1_output.close();
 evals2_output.close();
 rar_output.close();
 prices_output.close();
}


void compute_overlap(double alpha,int N, double sigma, double Delta, double pbar, int T, int nb_runs, int i=0, double eta = 0.0){

  double eps = 1e-9;
  std::string f1 = "rar";
  std::string f2 = "evals";
  std::string f3 = "overlaps";
  std::string f4 = "xi_matrix";

  std::string underscore = std::string("_");
  std::string sigma_float = std::to_string(sigma);
  sigma_float.erase( sigma_float.find_last_not_of('0') + 1, std::string::npos);

  std::string alpha_float = std::to_string(alpha);
  alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

  std::string to_append = underscore + sigma_float + underscore + alpha_float + underscore + std::to_string(N) + underscore;

  Perceptron test(alpha, N, sigma, Delta, pbar, eta);
  std::cout << "m = " << test.getm() << " \t" << "sigma = " << test.getsigma() << std::endl;
  std::ofstream rar_output(test.replacement_filename+to_append+std::to_string(i));
  std::ofstream evals1_output(test.evals1_filename+to_append+std::to_string(i));
  std::ofstream evals2_output(test.evals2_filename+to_append+std::to_string(i));
  std::ofstream overlap_output(f3+to_append+std::to_string(i));
  std::ofstream xi_output(f4+to_append+std::to_string(i));
  assert(overlap_output.is_open());
  assert(rar_output.is_open());
  assert(evals1_output.is_open());
  assert(evals2_output.is_open());
  assert(xi_output.is_open());
  test.compute_evals();
  evals1_output << test.evals1.real().transpose() << std::endl;
  evals2_output << test.evals2.real().transpose() << std::endl;
  rar_output << 0 << "\t" << test.z << "\t" << test.z*1.0/test.getM() << "\t" << test.energy << std::endl;
  xi_output << test.xi_matrix << std::endl;
  for (int j = 0; j<T; j++){
    test.run_sgd();
    rar_output << j+1 << "\t" << test.z << "\t" << test.z*1.0/test.getM() << "\t" << test.energy << std::endl;
    if (test.energy < eps){
      std::cout << "Energy reached zero. Stopping R&R procedure " << std::endl;
      break;
    }
    std::cout << "Replacing constraints now" << std::endl;

    test.replace_violated_constraints();
  }
  xi_output << test.xi_matrix << std::endl;
  test.compute_evals();
  evals1_output << test.evals1.real().transpose() << std::endl;
  evals2_output << test.evals2.real().transpose() << std::endl;
  evals1_output.close();
  evals2_output.close();
  rar_output.close();
  xi_output.close();
  std::cout << "Now running the overlap calculation" << std::endl;
  //omp_set_num_threads(nb_runs);
  //#pragma omp parallel for
  for (int k = 0; k < nb_runs; k++){
    Perceptron overlap(alpha,N,sigma, Delta, pbar, eta);
    overlap.xi_matrix = test.xi_matrix;
    overlap.run_sgd();
    double q = 0.0;
    q = test.prices.transpose()*overlap.prices;
    q = q/N;
    overlap_output << k+1 << "\t" << q << "\t"  << overlap.z << "\t" << overlap.z*1.0/overlap.getM() << "\t"  << overlap.energy << "\t" << test.energy << std::endl;
  }
  overlap_output.close();
}

void compute_overlap_gd(double alpha,int N, double sigma, double Delta, double pbar, int T, int nb_runs, int i=0, double eta = 0.0){

  double eps = 1e-9;
  int maxiter = 20000;
  std::string f1 = "rar";
  std::string f2 = "evals";
  std::string f3 = "overlaps";
  std::string f4 = "xi_matrix";

  std::string name = "gd"; 

  std::string underscore = std::string("_");
  std::string sigma_float = std::to_string(sigma);
  sigma_float.erase( sigma_float.find_last_not_of('0') + 1, std::string::npos);

  std::string alpha_float = std::to_string(alpha);
  alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

  std::string to_append = underscore + sigma_float + underscore + alpha_float + underscore + std::to_string(N) + underscore + name;

  Perceptron test(alpha, N, sigma, Delta, pbar, eta);
  std::cout << "m = " << test.getm() << " \t" << "sigma = " << test.getsigma() << std::endl;
  std::ofstream rar_output(test.replacement_filename+to_append+std::to_string(i));
  std::ofstream evals1_output(test.evals1_filename+to_append+std::to_string(i));
  std::ofstream evals2_output(test.evals2_filename+to_append+std::to_string(i));
  std::ofstream overlap_output(f3+to_append+std::to_string(i));
  std::ofstream xi_output(f4+to_append+std::to_string(i));
  assert(overlap_output.is_open());
  assert(rar_output.is_open());
  assert(evals1_output.is_open());
  assert(evals2_output.is_open());
  assert(xi_output.is_open());
  test.compute_evals();
  evals1_output << test.evals1.real().transpose() << std::endl;
  evals2_output << test.evals2.real().transpose() << std::endl;

  // First pass to get energy

  test.run_gd(maxiter);
  
  rar_output << 0 << "\t" << test.z << "\t" << test.z*1.0/test.getM() << "\t" << test.energy << std::endl;
  xi_output << test.xi_matrix << std::endl;
  for (int j = 1; j<T; j++){
    std::cout << "Replacing agents" << std::endl;
    test.replace_violated_constraints();

    //Re-run the gd
    test.run_gd(maxiter);
    rar_output << j << "\t" << test.z << "\t" << test.z*1.0/test.getM() << "\t" << test.energy << std::endl;
    if (test.energy < eps){
      std::cout << "Energy reached zero. Stopping R&R procedure " << std::endl;
      break;
    }
    std::cout << "Replacing constraints now" << std::endl;
  }
  xi_output << test.xi_matrix << std::endl;
  test.compute_evals();
  evals1_output << test.evals1.real().transpose() << std::endl;
  evals2_output << test.evals2.real().transpose() << std::endl;
  evals1_output.close();
  evals2_output.close();
  rar_output.close();
  xi_output.close();
  std::cout << "Now running the overlap calculation" << std::endl;
  //omp_set_num_threads(nb_runs);
  #pragma omp parallel for
  for (int k = 0; k < nb_runs; k++){
    Perceptron overlap(alpha,N,sigma, Delta, pbar, eta);
    overlap.xi_matrix = test.xi_matrix;
    overlap.run_gd(maxiter);
    double q = 0.0;
    q = test.prices.transpose()*overlap.prices;
    q = q/N;
    overlap_output << k+1 << "\t" << q << "\t"  << overlap.z << "\t" << overlap.z*1.0/overlap.getM() << "\t"  << overlap.energy << "\t" << test.energy << std::endl;
  }
  overlap_output.close();
}

