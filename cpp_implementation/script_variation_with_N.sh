T=1000
pbar=1.5
Delta=4.5
interval=50
sigma=-0.5
alpha=100

for N in 10 50 100 500 1000
do
    echo "Running with N=$N"
    ./one_script_to_rule_them_all.sh variation_with_N $alpha $sigma $pbar $Delta $N $T $interval
done
