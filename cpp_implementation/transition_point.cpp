#include "utils.hpp"

#include <cstring>

int main(int argc, char *argv[]){

  double alpha = 40.0;
  int copy = std::atoi(argv[1]);
  int N = std::atoi(argv[2]);
  double sigma = std::atof(argv[3]);
  double eta = 0.0;
  //double pbar = 0.0;
  double pbar = 1.5;
  double Delta = 4.5;

  Perceptron object(alpha, N, sigma, Delta, pbar, eta);

  int T = 500;
  object.rr_procedure(T, copy);
  
}

