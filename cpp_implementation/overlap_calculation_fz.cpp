#include "utils.hpp"
#include <cstring>

int main(int argc, char *argv[]){


  // Here we perform one pass of the R&R procedure till convergence and then run gd with different with different p keeping the same fixed converged xi_matrix.

  // At the end of t_stop, we will also save the eigenvalues of the Wishart matrix and compare it with the standard result given in the PNAS paper.

   //Parameters of the model

  double alpha = 20.0;
  int N = 50;
  double sigma = -1.0;
  double pbar = 1.5;
  //double pbar = 0.0;
  double Delta = 4.5;
  double eta = 0.0;

  const int nb_runs = 200;
  const int T = 1;
  const int copies = 1;
  #pragma omp parallel for
  for (int i = 0; i < copies; i++){
    compute_overlap(alpha, N, sigma, Delta, pbar, T, nb_runs, i, 0.0);
  }
}

