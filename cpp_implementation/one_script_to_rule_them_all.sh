if [[ "$#" -ne 8 ]]
then
    echo "Insufficient number of parameters"
    echo "Usage is ./one_script_to_rule_them_all <name of process> <alpha> <sigma> <pbar> <Delta> <N> <T> <interval>"
    exit
else
    echo "Running script now"
fi
   

PROCESSNAME=$1
alpha=$2
sigma=$3
pbar=$4
Delta=$5
N=$6
T=$7
verbosity=$8

queuename1=corei7c
queuename2=corei7b
UNDERSCORE="_"
CURRENTDATE=`date +"%Y-%m-%d %T"`
echo $CURRENTDATE

echo "$CURRENTDATE $PROCESSNAME $alpha $sigma $pbar $Delta $N $T $verbosity " >> logs.txt

CPPFILE=run_simulation
echo "Compiling file $CPPFILE"

OUTFILE=$CPPFILE$UNDERSCORE$alpha$UNDERSCORE$sigma$UNDERSCORE$N$UNDERSCORE$T

if [ -d "$PROCESSNAME" ]
then
    echo "$PROCESSNAME exists. No need to create a new directory"
else
    mkdir $PROCESSNAME
fi

g++ -I /users/sharma/installs/eigen -std=c++11 $CPPFILE.cpp -o $PROCESSNAME/$OUTFILE -O2 -march=native

echo "Executing file now"
cd $PROCESSNAME

qsub -cwd -m be -q $queuename1 -b y ./$OUTFILE $PROCESSNAME $alpha $sigma $pbar $Delta $N $T $verbosity

# ./$OUTFILE $PROCESSNAME $alpha $sigma $pbar $Delta $N $T $verbosity


