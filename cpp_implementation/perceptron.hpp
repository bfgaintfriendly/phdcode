//#include <omp.h>
#include <iostream>
#include <iomanip>
#include <map>
#include <random>
#include <math.h>
#include <string> 
#include <fstream>
#include <cassert>
#include <cstdlib> 
#define EIGEN_DEFAULT_TO_ROW_MAJOR
#include <Eigen/Dense>
#include <vector>
#include <functional>


typedef Eigen::VectorXd Vector;
typedef Eigen::MatrixXd Matrix;
class Perceptron
{
private:
  
  double alpha;
  int  N;
  int M;
  double Delta;
  double eta = 0.0;
  double sigma;
  double m;
  double pbar;

  double eps_p;
  double eps_d;
 

public:
  //xi matrix and prices are public since they are changed at every loop
  Matrix xi_matrix;
  Matrix xi_bar; // Barred matrix for transactions
  Vector prices;
  Vector budgets;
  Vector h_mu;
  Vector pi_mu;
  Vector pi_ema; // To store the ema of the money spent 
  Vector del_h;
  Vector sigma_vec;
  Vector constant;
  Vector supply; //storing total supply for each good
  Vector demand; //storing demand for each good 
  //Vector energy_list; //for storing energy values for GD loop
  //Vector norm_list; // for storing norm values in GD loop
  Eigen::VectorXi viocons; //Viocons for the true matrix xi
  Eigen::VectorXi viocons_ema; //Viocons vector for the matrix xi_bar. 
  
  Eigen::VectorXcd evals1; //without constraints
  Eigen::VectorXcd evals2; // with constraints;
  double energy = 0.0;
  double gradient_norm = 0.0;
  int z = 0;
  std::string prices_filename = "prices";
  std::string xi_filename = "xi_data"; 
  std::string base_filename = "energy_data";
  std::string replacement_filename = "rar";

  std::string evals1_filename = "evals_nc";
  std::string evals2_filename = "evals_wc"; 

  double step_size = 0.0001;
  
  
  //constructor
  Perceptron(double _alpha, int _N, double _sigma, double _Delta, double _pbar, double _eta = 1.0, double _eps_p = 0.05, double _eps_d = 0.05){
    alpha = _alpha;
    N = _N;
    Delta = _Delta;
    sigma = _sigma;
    pbar = _pbar;

    m = pbar/sqrt(Delta + pbar*pbar);

    M = alpha*N;

    eta = _eta;

    eps_p = _eps_p;
    eps_d = _eps_d;

    this->init_xi();
    this->init_prices();
    this->init_budgets();

    sigma_vec = Vector::Constant(M, sigma);
    this->viocons.setZero(M);
    constant = Vector::Constant(N,1.0);
    this->del_h.setZero();

    this->pi_mu.setZero(M);

    this->supply.setZero(N);
    this->demand.setZero(N);
    this->h_mu.setZero(M);

  }

  // Getters 
  int getM(){ return M; }
  int getN(){ return N; } 
  double getm(){ return m;}
  double getsigma(){ return sigma;}
  double getalpha(){ return alpha;}
  double getDelta(){ return Delta;}
  double getpbar() { return pbar;}
  double geteta() { return eta;}
  double getepsp() { return eps_p; }
  double getepsd() { return eps_d; }
  Vector getprices() { return prices;}
  Matrix getximatrix() { return xi_matrix;}
  
  //Setters
  
  void setalpha(double _alpha) {  alpha = _alpha;}
  void setsigma(double _sigma) { sigma = _sigma;}
  void setm(double _m) { m = _m;}
  void seteta(double _eta) { eta = _eta;} 
  

  void printxi(){
    std::cout << "Xi_matrix = " << xi_matrix << std::endl;}
  void printprices() {
    std::cout << "Prices = " << prices.transpose() << std::endl; }


  int generate_randint(int min, int max){
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<int> uni(min,max);

    return uni(rng);
  }

  double generate_delta(){
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_real_distribution<double> delta(0.0, 1.0);

    return delta(rng);
  }
  
  Vector generate_random_vector(int N, double mean, double variance = 1.0, int M = 1){
    //Function to generate a random Eigen Vector
    std::random_device rnd_device;
    std::mt19937 mersenne_engine(rnd_device());
    std::normal_distribution<double> dist(mean, variance);
    auto gen = std::bind(dist, mersenne_engine);
    std::vector <double> temp (M*N);
    std::generate(begin(temp), end(temp), gen);
    Vector new_vector = Eigen::Map<Vector, Eigen::Unaligned>(temp.data(), temp.size());
    return new_vector;
  } 

  void init_xi(){
    xi_matrix = this->generate_random_vector(N,0.0, 1.0, M);
    xi_matrix.resize(M,N);
  }

  void init_prices(){
    prices = this->generate_random_vector(N,m,1.0);
  }

  void init_budgets(){
    if (eta == 0.0){
      budgets.setZero(M);
    }
    else
      {
        budgets = this->generate_random_vector(M,0.0, eta, 1);
      }
  }

   double compute_gap_gd(){

     //this->h_mu.noalias() = (1.0/sqrt(N))*this->xi_matrix*this->prices -(this->sigma_vec+this->budgets);

     // We dont compute the complete gap just the money spent.
     // This method gives us the energy
     // We write another method for the EMA step
     this->h_mu = (1.0/sqrt(N))*this->xi_matrix*this->prices;
     
    double energy = 0.0;
    Vector temp(this->M);
    for (int i=0; i< this->h_mu.size(); ++i){
      double tem = this->h_mu(i)-this->sigma;
      if( tem < 0.0)
      {
        temp(i) = tem;
        energy = energy +  (tem*tem);
        this->viocons(i) = 1;
      }
      else{
        temp(i) = 0.0;
        this->viocons(i) = 0;
      }
    }
    // std::cout << "Temp : " <<temp<< std::endl;
    this->del_h.noalias() = (1.0/sqrt(this->N))*temp.transpose()*this->xi_matrix;

    energy = (0.5/N)*energy;
    this->z = this->viocons.sum();
    return energy; 
    // std::cout << "Energy = " << energy << std::endl;
    // std::cout << "Gradient = " << del_h << std::endl;
  }

  int compute_violated_constraints(){
    for (int i = 0; i <this->h_mu.size(); i++){
      double tem = this->h_mu(i);
      if (tem  >  0.0){
        this->viocons(i) = 0;
      }
      else{
        this->viocons(i) = 1;
      }
    }
    this->z = this->viocons.sum();
    return (int) this->z;
  }

  void run_gd(int maxiter, int period = 1000, bool verbose=true)
  {

    //We set the energy and norm lists to zero at every fresh run of the GD loop
    //energy_list.setZero();
    //norm_list.setZero();

    double lambda_constraint = 55000;
    double mu_constraint = 55000;
    int iteration_number = 0;

    double del_norm = 1.0e6;
    double energy = 0.0;
    double eps = 1e-9;

    // while (del_norm > 1.0e-6) && (lambda_constraint <= 1.0e7 && mu_constraint <= 1.0e7) && (iteration_number < 200000){
   do {
      
      this->energy = this->compute_gap_gd();
      // if (this->energy < eps){
      //   std::cout << "Energy is zero. Stopping Gd" << std::endl;
      //   break;
      // }
        
      //std::cout << "energy " << energy << std::endl;
      double prices_norm = this->prices.squaredNorm()/(this->N);
      double prices_sum = this->prices.sum()/(this->N);
      double prefac1 = (2*lambda_constraint/this->N)*(prices_norm- 1);
      double prefac2 = (mu_constraint/this->N)*(prices_sum - this->getm());
      //std::cout << "Checking whether gap computation works " << std::endl;
      Vector complete_gradient = this->del_h + this->prices*prefac1 + Vector::Constant(this->N, prefac2);
      // this->del_h = this->del_h +  prefac1*(this->prices) + prefac2*(this->constant);
      this->prices = this->prices -  complete_gradient*step_size;
      del_norm = complete_gradient.squaredNorm();
      if (verbose){
        if (iteration_number%period == 0.0){
          std::cout << "------Iteration number :  " << iteration_number << " ---------" << std::endl;
          std::cout << " Energy = " << this->energy << " Average prices = " << prices_sum << " Average squared = " << prices_norm <<  " Z = " << this->z << std::endl;
           std::cout << "Del Norm = " << del_norm << std::endl;
        }
      }
     
      if (iteration_number % maxiter == 0.0 && iteration_number >0 ){
        std::cout << "------Lambda and mu changing ----- "<<std::endl;
        lambda_constraint = lambda_constraint * 1.1;
        mu_constraint = mu_constraint * 1.1;
        std::cout << "Lambda = " << lambda_constraint << std::endl;
      }       
      //this->energy_list.conservativeResize(iteration_number+1);
      //this->energy_list(iteration_number) = energy;
      //this->norm_list.conservativeResize(iteration_number+1);
      //this->norm_list(iteration_number) = del_norm;
      iteration_number +=1;
    } while(del_norm > 1e-6 && lambda_constraint < 1.0e7 && mu_constraint < 1.0e7 && iteration_number < 200000);
   //this->energy = energy;
    std::cout<< "Convergence!! "<< std::endl;
    std::cout <<"Iteration_number " << iteration_number <<  " Energy = " <<this->energy << " Average prices = " << this->prices.sum()/this->N << " Average squared = " << this->prices.squaredNorm()/this->N << " Del Norm = " << del_norm << std::endl;

    this->gradient_norm = del_norm;
  }

  void replace_violated_constraints(){
    //generate a random vector with the same distribution as the Xi_matrix
    for (int i = 0; i < this->viocons.size(); i++){
      if (this->viocons(i) != 0){
        this->xi_matrix.row(i) = this->generate_random_vector(N,0.0);
      }
    }
    // this->z = 0;
    // this->viocons.setZero();
    // this->h_mu.setZero();
    // this->energy = 0.0;
  }

  void replace_worst_constraint(){
    //remove the worst-performing agent
    Eigen::VectorXd::Index  min_row, min_col;
    double min = this->h_mu.minCoeff(&min_row, &min_col);
    this->xi_matrix.row(min_row) = this->generate_random_vector(N, 0.0);
  }

  void compute_evals(){
    Matrix corr = this->xi_matrix.transpose()*this->xi_matrix;
    corr = corr/this->M;
    Matrix xi_zero = this->xi_matrix;
    this->energy = this->compute_gap_gd();
    for (int i = 0; i < this->viocons.size(); i++){
      if (this->viocons(i) !=0){
        xi_zero.row(i).setZero();
      }
    }

    evals1 = corr.eigenvalues();
    corr = this->xi_matrix.transpose()*xi_zero;
    corr = corr/this->M;
    evals2 = corr.eigenvalues();
  }

  void rr_procedure(int T, int copy){
    double eps = 1e-9;
    int maxiter = 20000;
    std::string underscore = std::string("_");
    std::string sigma_float = std::to_string(this->sigma);
    sigma_float.erase(sigma_float.find_last_not_of('0') + 1, std::string::npos);

    std::string alpha_float = std::to_string(this->alpha);
    alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

    std::string name = "gd";
    std::string to_append = underscore + sigma_float + underscore + alpha_float + underscore + std::to_string(N) + underscore + name;

    std::ofstream rar_output(this->replacement_filename+to_append+std::to_string(copy));
    //std::ofstream evals1_output(this->evals1_filename+to_append+std::to_string(copy));
    //std::ofstream evals2_output(this->evals2_filename+to_append+std::to_string(copy));
    std::ofstream prices_output(this->prices_filename+to_append+std::to_string(copy));
    std::ofstream xi_output(this->xi_filename+to_append + std::to_string(copy));
 
    //std::ofstream overlap_output(f3+std::to_string(i));
    //assert(overlap_output.is_open());
    assert(rar_output.is_open());
    //assert(evals1_output.is_open());
    //assert(evals2_output.is_open());
    assert(prices_output.is_open());
    assert(xi_output.is_open());
    //this->compute_evals();
    //evals1_output << this->evals1.real().transpose() << std::endl;
    //evals2_output << this->evals2.real().transpose() << std::endl;
    xi_output << this->xi_matrix << std::endl;
    prices_output << this->prices.transpose() << std::endl;
 //First pass to get energy
    this->run_gd(maxiter);
 // We save the price vector 
    prices_output << this->prices.transpose() << std::endl;
 // And now the stats
    rar_output << 0 << "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << this->energy << std::endl;
    int j = 1;
 // Now we use the remove and replace procedure
    while (j <= T) {
      std::cout << "Replacing agents" << std::endl;
      this->replace_violated_constraints();
      //Re-run the GD
      this->run_gd(maxiter);
      rar_output << j  << "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << this->energy << std::endl;
      prices_output << this->prices.transpose() << std::endl;
      //j+=1;
      if (this->energy < eps){
        std::cout << "Energy reached zero. Stopping R&R procedure " << std::endl;
        break;
      }

      if (j%50 == 0 && j >0){
        this->compute_evals();
        // evals1_output << this->evals1.real().transpose() << std::endl;
        // evals2_output << this->evals2.real().transpose() << std::endl;
        xi_output << this->xi_matrix << std::endl;
      }

      j+=1;
    }
    if (j != T){
      this->compute_evals();
      // evals1_output << this->evals1.real().transpose() << std::endl;
      // evals2_output << this->evals2.real().transpose() << std::endl;
      xi_output << this->xi_matrix << std::endl;
    }
    xi_output.close();
    //evals1_output.close();
    //evals2_output.close();
    rar_output.close();
    prices_output.close();
  }

  

  void planted_solution(std::string filename, int nb_runs, int copy=0){

    Eigen::VectorXd fixed_prices(N);
    std::ifstream in (filename, std::ios::in);
    for (int i=0; i < N; i++){
      in >> fixed_prices(i);
    }
    in.close();

    this->prices = fixed_prices.transpose();    
    std::string underscore = std::string("_");
    std::string sigma_float = std::to_string(this->sigma);
    sigma_float.erase(sigma_float.find_last_not_of('0') + 1, std::string::npos);

    std::string alpha_float = std::to_string(this->alpha);
    alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

    std::string name = "planted";
    std::string to_append = underscore + sigma_float + underscore + alpha_float + underscore + std::to_string(N) + underscore + name;

    std::ofstream rar_output(this->replacement_filename+to_append+std::to_string(copy));
    std::ofstream xi_output(this->xi_filename+to_append + std::to_string(copy));
    assert(rar_output.is_open());
    assert(xi_output.is_open());
    xi_output << this->xi_matrix << std::endl;
    
    this->energy = this->compute_gap_gd();
    
    rar_output << 0 << "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << this->energy << std::endl;
    int j = 0;
 // Now we use the remove and replace procedure
    while (j < nb_runs) {
      std::cout << "Replacing agents" << std::endl;
      this->replace_violated_constraints();
      //Re-run the gap computation
      this->energy = this->compute_gap_gd();
      std::cout << j+1  << "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << this->energy << std::endl;
      rar_output << j+1  << "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << this->energy << std::endl;
      //prices_output << this->prices.transpose() << std::endl;
      //j+=1;
      if (this->energy == 0){
        std::cout << "Energy reached zero. Stopping R&R procedure " << std::endl;
	std::cout << "Energy " << this->energy << " z " << this->z << std::endl;
        break;
      }

      j+=1;
    }
    // if (j != nb_runs){
    //   this->compute_evals();
    //   // evals1_output << this->evals1.real().transpose() << std::endl;
    //   // evals2_output << this->evals2.real().transpose() << std::endl;
    //   xi_output << this->xi_matrix << std::endl;
    // }
    xi_output << this->xi_matrix << std::endl;
    xi_output.close();
    //evals1_output.close();
    //evals2_output.close();
    rar_output.close();
    //prices_output.close();
  }


  void aggregate_demand_supply(){
    //Populate the demand and supply vectors 
    for (int i = 0; i < N; i++){
      Vector test = this->xi_matrix.col(i);
    //std::cout << "Working with column number " << i << std::endl;
    //std::cout << "Size of column " << test.size() << std::endl;
      double temp_supply = 0.0;
      double temp_demand = 0.0;
      for (int j = 0; j  < test.size(); j++){
	bool buy_sell = (test(j) > 0);
	if (buy_sell){
	  temp_supply += test(j);
	}
	else {
	  temp_demand += test(j);
	}
      }
    //std::cout << "Supply " << temp_supply << " Demand" << temp_demand << std::endl;
    this->supply(i) = temp_supply;
    this->demand(i) = temp_demand;
    }
  }
  
  void update_demand_matrix(){
  //Now update the matrix

    for (int i = 0; i< this->M; i++){
      for (int j = 0; j < this->N; j++){
	double val = this->xi_matrix(i,j);
	if( val  > 0){
	  //std::cout << "Agent is a seller" << std::endl;
	  if (this->supply(j) > abs(this->demand(j))){
	  //std::cout << "Supply greater than demand " << std::endl;
	    double delta = this->generate_delta();
	    double temp = val*(1-this->eps_d*delta);
	    this->xi_matrix(i,j) = temp;
	  //std::cout << val <<"\t" << temp << std::endl;
	  }
	  
	  else{
	  //std::cout << "Demand greater than supply" << std::endl;
	    double delta = this->generate_delta();
	    double temp = val*(1+this->eps_d*delta);
	    this->xi_matrix(i,j) = temp;
	  //	  std::cout << val <<"\t" << temp << std::endl;
	  }
	}
	else{
	//	std::cout << "Agent is a buyer " << std::endl;
	  if (this->prices(j) > this->getm()){
	  //std::cout << "Good too expensive " << std::endl;
	    double delta = this->generate_delta();
	    double temp = val*(1 -this->eps_p*delta);
	    this->xi_matrix(i,j) = temp;
	  //	  std::cout << val <<"\t" << temp << std::endl;
	  }
	  else{
	  //	  std::cout << "Good is cheaper " << std::endl;
	    double delta = this->generate_delta();
	    double temp = val*(1+this->eps_p*delta);
	    this->xi_matrix(i,j) = temp;
	  //	  std::cout << val <<"\t" << temp << std::endl;
	  }
	}
      }
    }
  }

  void perform_transactions(){

    // Function which performs the transaction step.
    // Computes xi_bar based on the supply-demand mismatch

    // 1. First we re-compute the supply and demand vectors

    this->aggregate_demand_supply();

    // 2. We go over the loop in
    this->xi_bar = Eigen::MatrixXd::Zero(this->M, this->N); // Correct way to initialize a zero matrix in eigen
    
    for (int i = 0; i < this->M; i++){
      for (int j = 0; j < this->N; j++){
	double zeta = this->supply(j) / (std::abs(this->demand(j)));
	//std::cout << "Zeta: " << zeta << " Demand:  " << this->xi_matrix(i,j);
	// print "Zeta :{}, Pref: {}".format(zeta, copy2[i,j]) 
	if (zeta > 1 && this->xi_matrix(i,j) > 0){
	  // print "Supply greater than demand. Agent is seller."
	  //std::cout << "Supply greater than demand. Agent is seller. " << i << "\t" << j << std::endl;
	  xi_bar(i,j) = this->xi_matrix(i,j) / zeta;
	}
	else if (zeta < 1 && this->xi_matrix(i,j) < 0){
	  //std::cout << "Supply less than demand. Agent is buyer." << i << "\t" << j << std::endl;
	  xi_bar(i,j) = this->xi_matrix(i,j) * zeta;
	}
	else{
	  //std::cout << "Nothing to do here " << i << "\t" << j << std::endl;
	  xi_bar(i,j) = this->xi_matrix(i,j);
	}
      }
    }
    // return xi_bar;
  }

  void init_soft_rr(){
    // This is run just once to initialize the pi_ema vector

    this->pi_mu = (1.0/sqrt(N))*this->xi_bar*this->prices;
    this->viocons_ema.setZero(this->M);
    this->pi_ema.setZero(this->M);
    for (int i =0; i < this->M; i++){
      this->pi_ema(i) =  this->pi_mu(i);
    }
  }

  void init_hard_rr(){
    // this->h_mu = (1.0/sqrt(N))*this->xi_matrix*this->prices;
    this->pi_mu = (1.0/sqrt(N))*this->xi_bar*this->prices;

    this->viocons_ema.setZero(this->M);
    this->pi_ema.setZero(this->M);

    for (int i = 0; i < this->M; i++){
      if (this->pi_mu(i) < this->sigma){
	this->xi_matrix.row(i) = this->generate_random_vector(this->N, 0.0);
	this->viocons_ema(i) = 1;
      }
      else{
	this->pi_ema(i) = this->pi_mu(i);
      }
    }
  }

  void hard_ema_rr(double omega){
    // We recompute the h_mu vector
    // this->h_mu = (1.0/sqrt(N))*this->xi_matrix*this->prices;
    this->pi_mu = (1.0/sqrt(N))*this->xi_bar*this->prices;
    // int init_sum = this->viocons_ema.sum();
    // int count_viocons = 0;
    for (int i = 0; i < this->M; i++){
      if (this->viocons_ema(i) == 0){
        this->pi_ema(i) = omega*this->pi_mu(i) + (1-omega)*this->pi_ema(i);
	if (pi_ema(i) < this->sigma){
	  this->viocons_ema(i) = 1;
	  //std::cout << "Viocon changed from 0 to 1 "<< std::endl;
	  //std::cout << "Replacing agent"<< std::endl;
	  this->xi_matrix.row(i) = this->generate_random_vector(this->N,0.0);
	}
	else{
	  //std::cout << "Viocon remains the same (0)" << std::endl;
	}
      }
      else{
        //count_viocons = count_viocons + 1;
	this->pi_ema(i) = this->pi_mu(i);
	if (this->pi_ema(i) < sigma){
	  //std::cout << "Viocon remains the same (1)"<< std::endl;
	  this->viocons_ema(i) = 1;
	  //std::cout << "Replacing agent" << std::endl;
	  this->xi_matrix.row(i) = this->generate_random_vector(this->N, 0.0);
	}
	else{
	  //std::cout << "Viocons changed from 1 to 0 "<< std::endl;
	  this->viocons_ema(i) = 0;
	}
      }
    }
    //std::cout << "Initial sum " << init_sum << " Final loop count " << count_viocons << std::endl;
    
  }

  void soft_ema_rr(double omega){

    this->pi_mu = (1.0/sqrt(N))*this->xi_bar*this->prices;
    for (int i = 0; i < this->M; i++){

      if(this->viocons_ema(i) == 0){
	// we update the pi_
	this->pi_ema(i) = omega*this->pi_mu(i) + (1-omega)*this->pi_ema(i);
	if (pi_ema(i) < this->sigma){
	  // std::cout << "Viocons changed from 0 to 1" << std::endl;
	  this->viocons_ema(i) = 1;
	  //std::cout << "Replacing agent " << std::endl;
	  this->xi_matrix.row(i) = this->generate_random_vector(this->N, 0.0);
	}
	else{
	  //std::cout << "Viocons remain the same" << std::endl;
	}
      }
      else{
	//std::cout << "Agent was removed in previous step" << std::endl;
	//std::cout << "Previous vcons " << this->viocons_ema(i);
	this->pi_ema(i) = this->pi_mu(i);
	this->viocons_ema(i) = 0;
	//std::cout << " New viocons " << this-> viocons_ema(i) << std::endl;
      }

      //std::cout << " ------------------------ " << std::endl;
    }
  }
  void dynamics_new(int T, int copy=0){

    // Save the xi matrix in binary format after every update of the matrix
    // Save the price vector after every gradient descent procedure 
    // T is the number of timesteps taken for the procedure.

    //Setting up the files to write
    int maxiter = 20000;
    std::string underscore = std::string("_");
    std::string sigma_float = std::to_string(this->sigma);
    sigma_float.erase(sigma_float.find_last_not_of('0') + 1, std::string::npos);

    std::string alpha_float = std::to_string(this->alpha);
    alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

    std::string eps_float = std::to_string(this->eps_p);
    eps_float.erase( eps_float.find_last_not_of('0') + 1, std::string::npos);

    std::string name = "new_dyn";
    std::string to_append = underscore + sigma_float + underscore + alpha_float + underscore + std::to_string(N) + underscore + eps_float + underscore + name;
    

    std::ofstream rar_output(this->replacement_filename+to_append+std::to_string(copy));
    std::ofstream prices_output(this->prices_filename+to_append+std::to_string(copy));
    //std::ofstream supply_output("supply"+to_append+std::to_string(copy));
    //    std::ofstream demand_output("demand" + to_append + std::to_string(copy));
    // Saving the matrix in binary format
    // Updated the code to save only starting and ending xi_matrices
    std::ofstream xi_output(this->xi_filename+to_append + std::to_string(copy));

    assert(rar_output.is_open());
    assert(prices_output.is_open());
    assert(xi_output.is_open());
    //assert(supply_output.is_open());
    //    assert(demand_output.is_open());
    

    // long rows_xi = this->getM();
    // long cols_xi = this->getN();
    // long size_xi = rows_xi * cols_xi;
    // xi_output.write( (char *) (&rows_xi), sizeof(rows_xi));
    // xi_output.write( (char *) (&cols_xi), sizeof(cols_xi));
    //xi_output.write( (char *) this->xi_matrix.data(), size_xi*sizeof(typename Eigen::MatrixXd::Scalar));
    
    //xi_output << this->xi_matrix << std::endl;
    //prices_output << this->prices.transpose() << std::endl;

    // Update the demand matrix

    // Initial random configurations

    double temp = this->compute_gap_gd() ;
    rar_output << 0 <<  "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << temp  << std::endl;
    xi_output << this->xi_matrix << std::endl;
    prices_output << this->prices.transpose() << std::endl;
    
    this->aggregate_demand_supply();
    //demand_output << this->demand.transpose() << std::endl;
    //supply_output << this->supply.transpose() << std::endl;
    this->update_demand_matrix();
    //xi_output.write( (char *) this->xi_matrix.data(), size_xi*sizeof(typename Eigen::MatrixXd::Scalar));

    // We save the xi_matrix, this is in some sense the true starting point

    xi_output << this->xi_matrix << std::endl;
    // Now run_gd

    this->run_gd(maxiter);

    //save prices vector 
    prices_output << this->prices.transpose() << std::endl;

    //save energy and other details. 
    rar_output << 1 <<  "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << this->energy << std::endl;

    for (int j = 2; j < T+1; j++){
      // Here we dont need the while loop since we are going to let the economy evolve as is
      // even when the energy reaches zero. 
      
      std::cout << "Removing agents " << std::endl;
      this->replace_violated_constraints();     
      std::cout << "Update agents" << std::endl;
      this->aggregate_demand_supply();
      //demand_output << this->demand.transpose() << std::endl;
      //      supply_output << this->supply.transpose() << std::endl;

      this->update_demand_matrix();

      //      xi_output.write( (char *) this->xi_matrix.data(), size_xi*sizeof(typename Eigen::MatrixXd::Scalar));
      this->run_gd(maxiter);

      prices_output << this->prices.transpose() << std::endl;

      rar_output << j << "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << this->energy << std::endl;
      // if (this->energy == 0){
      // 	  std::cout << "Energy reached zero. Stopping the dynamics " << std::endl;
      // 	  std::cout << "Energy " << this->energy << " z " << this->z << std::endl;
      // 	  break;
      // }
    }
    //xi_output << this->xi_matrix << std::endl;
    xi_output << this->xi_matrix << std::endl;
    xi_output.close();
    prices_output.close();
    rar_output.close();
    //demand_output.close();
    //    supply_output.close();

  }
  

  void dynamics_ema(int T, double omega,int copy = 0, bool hard = true, int interval = 10){
    // Interval is the number of steps after which we store the xi matrix. 
    int maxiter = 20000;
    std::string underscore = std::string("_");
    std::string sigma_float = std::to_string(this->sigma);
    sigma_float.erase(sigma_float.find_last_not_of('0') + 1, std::string::npos);

    std::string alpha_float = std::to_string(this->alpha);
    alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

    std::string eps_float = std::to_string(this->eps_p);
    eps_float.erase( eps_float.find_last_not_of('0') + 1, std::string::npos);

    // Must add omega tag to know which value of omega is being used 
    std::string omega_float = std::to_string(omega);
    omega_float.erase(omega_float.find_last_not_of('0') + 1, std::string::npos);

    std::string name = "new_dyn_ema";
    std::string to_append = underscore + sigma_float + underscore + alpha_float + underscore + std::to_string(N) + underscore + eps_float + underscore + omega_float + underscore + name;
    

    std::ofstream rar_output(this->replacement_filename+to_append+std::to_string(copy));
    std::ofstream prices_output(this->prices_filename+to_append+std::to_string(copy));
    //std::ofstream supply_output("supply"+to_append+std::to_string(copy));
    //    std::ofstream demand_output("demand" + to_append + std::to_string(copy));
    // Saving the matrix in binary format
    // Updated the code to save only starting and ending xi_matrices
    std::ofstream xi_output(this->xi_filename+to_append + std::to_string(copy));

    assert(rar_output.is_open());
    assert(prices_output.is_open());
    assert(xi_output.is_open());

    double temp = this->compute_gap_gd() ;
    rar_output << 0 <<  "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << temp  << std::endl;
    xi_output << this->xi_matrix << std::endl;
    prices_output << this->prices.transpose() << std::endl;
    
    this->aggregate_demand_supply();
    //demand_output << this->demand.transpose() << std::endl;
    //supply_output << this->supply.transpose() << std::endl;
    this->update_demand_matrix();
    // We save the xi_matrix, this is in some sense the true starting point

    xi_output << this->xi_matrix << std::endl;
    // Now run_gd

    this->run_gd(maxiter);

    //save prices vector 
    prices_output << this->prices.transpose() << std::endl;


    // perform transactions
    this->perform_transactions();

    // Initialize the ema_stuff
    if (hard){
      //hard R&R
      std::cout << "Running hard ema r&r" << std::endl;
      this->init_hard_rr();
    }
    else{
      std::cout << "Running soft ema r&r" << std::endl;
      this->init_soft_rr();
    }

    int z_ema = 0;
    z_ema = this->viocons_ema.sum();
    
    //save energy, z and z_ema 
    rar_output << 1 <<  "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << this->energy << "\t" << z_ema << "\t" << z_ema*1.0/this->getM() << "\t" << this->gradient_norm << std::endl;

    // Loop

    for (int j = 2; j < T +1; j++){

      // Aggregate supply
      this->aggregate_demand_supply();

      //Update demand matrix
      this->update_demand_matrix();

      //Run GD

      this->run_gd(maxiter);

      prices_output << this->prices.transpose() << std::endl;

      if (j % interval == 0 ){
	// Save the xi matrix at intervals of <interval> 
	xi_output << this->xi_matrix << std::endl;
      }
      

      //Perform transactions
      this->perform_transactions();

      //Do EMA step;
      if (hard){
	this->hard_ema_rr(omega);
      }
      else {
	this->soft_ema_rr(omega);
      }

      z_ema = this->viocons_ema.sum();
    
    //save energy, z and z_ema 
      rar_output << j <<  "\t" << this->z << "\t" << this->z*1.0/this->getM() << "\t" << this->energy << "\t" <<  z_ema << "\t" << z_ema*1.0/this->getM() << "\t" << this->gradient_norm << std::endl;
    }

    xi_output << this->xi_matrix << std::endl;
    xi_output.close();
    prices_output.close();
    rar_output.close();
  }

      

};


  
