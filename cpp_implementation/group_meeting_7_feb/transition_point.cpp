#include "../utils.hpp"

int main(int argc, char *argv[]){

  double alpha = 50.0;
  int N = 200;
  double eta = 0.0;
  double Delta = 4.5;
  int T = 200;
  int nb_copies = 2;


  double sigma = std::atof(argv[1]);
  double pbar = std::atof(argv[2]);
  // Vector sigma_vals(7);

  // sigma_vals << -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5;

  // omp_set_num_threads(sigma_vals.size());
  rr_procedure(alpha, N, sigma, Delta, pbar, T, nb_copies, 0.0);
  
  // #pragma omp parallel for
  // for (int j = 0; j < sigma_vals.size(); j++){
  //   rr_procedure(alpha, N, sigma_vals(j), Delta, pbar, T, nb_copies, 0.0);
  // }
}
