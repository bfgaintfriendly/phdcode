#include "../utils.hpp"

int main(int argc, char *argv[]){

  double alpha = 20.0;
  int N = 200;
  double eta = 0.0;
  double Delta = 4.5;
  int T = 400;

  int nb_runs = 500;
  int copy_number = std::atoi(argv[1]);
  double sigma = std::atof(argv[2]);
  double pbar = std::atof(argv[3]);
  double eps = 1e-9;
  int maxiter = 20000;
  std::string underscore = std::string("_");
  std::string sigma_float = std::to_string(sigma);
  sigma_float.erase( sigma_float.find_last_not_of('0') + 1, std::string::npos);

  std::string alpha_float = std::to_string(alpha);
  alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

  std::string to_append = underscore + sigma_float + underscore + alpha_float + underscore + std::to_string(N) + underscore;

  
  std::string f1 = "rar";
  std::string f2 = "evals";
  std::string f3 = "overlaps_before";
  std::string f4 = "overlaps_after";
  std::string f5 = "xi_matrix";
  std::string f6 = "xi_normalized";

  std::string f7 = "prices_before";

  std::string f8 = "prices_after"; 
  std::string f9 = "prices_during"; 
  
  std::ofstream xi1(f5+to_append+std::to_string(copy_number));
  std::ofstream xi2(f6+to_append+std::to_string(copy_number));

  std::ofstream o1(f3+to_append+std::to_string(copy_number));
  std::ofstream o2(f4+to_append+std::to_string(copy_number));

  std::ofstream p1(f7+to_append+std::to_string(copy_number));
  std::ofstream p2(f8+to_append+std::to_string(copy_number));
  std::ofstream p3(f9+to_append+std::to_string(copy_number));
  

  assert(xi1.is_open());
  assert(xi2.is_open());
  assert(o1.is_open());
  assert(o2.is_open());

  assert(p1.is_open());
  
  Perceptron object(alpha, N, sigma, Delta, pbar, eta);
  Matrix copy = normalize_cols(object);
  xi1 << object.xi_matrix << std::endl;
  xi2 << copy << std::endl;

  //Running the procedure with initial unchanged xi_matrix
  for (int i = 0; i < nb_runs; i++){
    object.init_prices();
    object.run_gd(maxiter);
    p1 << object.prices.transpose() << std::endl;
    o1 << i+1 << "\t" << object.z << "\t" << object.z*1.0/object.getM() << "\t" << object.energy<< std::endl;
  }

  p1.close();
  o1.close();  
  
  //Now performing the R&R procedure

  std::cout << "Running the repeal and replace procedure" << std::endl;
  std::ofstream rar_output(object.replacement_filename+to_append+std::to_string(copy_number));
  std::ofstream evals1_output(object.evals1_filename+to_append+std::to_string(copy_number));
  std::ofstream evals2_output(object.evals2_filename+to_append+std::to_string(copy_number));
  //std::ofstream overlap_output(f3+std::to_string(i));
  //assert(overlap_output.is_open());
  assert(rar_output.is_open());
  assert(evals1_output.is_open());
  assert(evals2_output.is_open());
  assert(p3.is_open());
  object.compute_evals();
  evals1_output << object.evals1.real().transpose() << std::endl;
  evals2_output << object.evals2.real().transpose() << std::endl;
  object.init_prices();

  // First pass to get energy
  object.run_gd(maxiter);
  //Now we have changed the prices and thus we save this price vector
  p3 << object.prices.transpose() << std::endl;
  // We now save the stats that we want
  rar_output << 0 << "\t" << object.z << "\t" << object.z*1.0/object.getM() << "\t" << object.energy << std::endl;
  
  for (int j = 1; j<=T; j++){
    std::cout << "Replacing agents" << std::endl;
    object.replace_violated_constraints();
    // Re-run the GD
    object.run_gd(maxiter);
    rar_output << j << "\t" << object.z << "\t" << object.z*1.0/object.getM() << "\t" << object.energy << std::endl;
    p3 << object.prices.transpose() << std::endl;
    if (object.energy < eps){
      std::cout << "Energy reached zero. Stopping R&R procedure " << std::endl;
      break;
    }

    if (j%50 == 0 && j >0){
      object.compute_evals();
      xi1 << object.xi_matrix << std::endl;
      copy = normalize_cols(object);
      xi2 << copy << std::endl;
      evals1_output << object.evals1.real().transpose() << std::endl;
      evals2_output << object.evals2.real().transpose() << std::endl;
    }
  }
  
  xi1 << object.xi_matrix << std::endl;
  copy = normalize_cols(object);
  xi2 << copy << std::endl;
  object.compute_evals();
  evals1_output << object.evals1.real().transpose() << std::endl;
  evals2_output << object.evals2.real().transpose() << std::endl;
  evals1_output.close();
  evals2_output.close();
  rar_output.close();
  xi1.close();
  xi2.close();
  p3.close();

  assert(p2.is_open());
  
  //Finally the overlap computation
  std::cout << "Now running the overlap calculation" << std::endl;
  for (int k = 0; k < nb_runs; k++){
    Perceptron overlap(object.getalpha(),object.getN(),object.getsigma(), object.getDelta(), object.getpbar(), object.geteta());
    overlap.xi_matrix = object.xi_matrix;
    overlap.run_gd(maxiter);
    double q = 0.0;
    p2 << overlap.prices.transpose() << std::endl;
    q = object.prices.transpose()*overlap.prices;
    q = q/N;
    o2 << k+1 << "\t" << q << "\t"  << overlap.z << "\t" << overlap.z*1.0/overlap.getM() << "\t"  << overlap.energy << "\t" << object.energy << std::endl;
  }
  o2.close();
  p2.close();

  
}
  
