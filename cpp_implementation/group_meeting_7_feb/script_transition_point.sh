mkdir trans0
mkdir trans0.57

#echo "Compiling file for m=0"
#g++ -I ~/installs/eigen -std=c++11 transition_point.cpp -o trans0/transition_point_m_0 -O2 -march=native -fopenmp
#cd trans0
#for sigma in -1.5 -1.0 -0.5 0.0 0.5 1.0 1.5
#do
 #   echo "Running with sigma = $sigma"
  #  OMP_NESTED=TRUE ./transition_point_m_0 $sigma 0
   # command; echo "Run completed"| mail -s "Progress for m=0. Sigma = $sigma" sharma@lpt.ens.fr
#done

#cd ..
echo "Compiling file for m = 0.57"
g++ -I ~/installs/eigen -std=c++11 transition_point.cpp -o trans0.57/transition_point_m_0.57 -O2 -march=native -fopenmp
cd trans0.57
for sigma in -1.5 -1.0 -0.5 -0.52 0.0 0.5 1.0 1.5
do
    echo "Running with sigma = $sigma"
    OMP_NESTED=TRUE ./transition_point_m_0.57 $sigma 1.5
    command; echo "Run completed"| mail -s "Progress for m=0.57.  Sigma = $sigma" sharma@lpt.ens.fr
done
