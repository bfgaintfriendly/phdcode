# mkdir overlap_m0
# mkdir overlap_m0.57

echo "Compiling file for m=0"

g++ -I ~/installs/eigen -std=c++11 overlap_calculation_fz.cpp -o onewgd_m0/run_m0 -O2 -march=native -fopenmp

echo "Compiling file for m=0.57"

g++ -I ~/installs/eigen -std=c++11 overlap_calculation_fz.cpp -o onewgd_m0.57/run_m0.57 -O2 -march=native -fopenmp

# cd overlap_m0

# for sigma in -1.0 1.0
# do
#     echo "Running with sigma = $sigma"
#     qsub -cwd -m be -b y -q corei7c ./run_m0 $sigma 0
#     # command; echo "Overlap calculation"| mail -s "Progress for m=0. Sigma = $sigma" sharma@lpt.ens.fr 
# done

# cd ..
# cd overlap_m0.57

# for sigma in -1.0 1.0
# do
#     echo "Running with sigma = $sigma"
#     qsub -cwd -m be -b y -q corei7c ./run_m0.57 $sigma 1.5
#     # command; echo "Overlap calculation" | mail -s "Progress for m=0.57. Sigma = $sigma" sharma@lpt.ens.fr
# done
