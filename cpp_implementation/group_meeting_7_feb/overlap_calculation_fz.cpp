#include "../utils.hpp"

int main (int argc, char *argv[]){

  //double alpha = 20.0;
  int N = 200;
  double eta = 0.0;
  double Delta = 4.5;
  int T = 400;

  int nb_runs = 500;

  double sigma = std::atof(argv[1]);
  double pbar = std::atof(argv[2]);
  double alpha = std::atof(argv[3]);
  int copy = std::atoi(argv[4]);
  //compute_overlap_gd(alpha, N, sigma, Delta, pbar, T, nb_runs);

  Perceptron test(alpha, N, sigma, Delta, pbar, eta);
  test.rr_procedure(T, copy);
}


