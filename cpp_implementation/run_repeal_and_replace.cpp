#include "perceptron.hpp"
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring> 
int main(int argc, char *argv[]){
  int T = 100;
  double alpha = atof(argv[1]);
  double sigma = atof(argv[2]);
  int N = 0;
  if (argc >3){
    N = atoi(argv[3]);
    T = atoi(argv[4]);
  }
  else {
    N = 100;
    T = 100;
  }
  
  double Delta = 4.5;
  double pbar = 1.5;
  double eta = 0.0;

  Perceptron test(alpha, N, sigma, Delta, pbar, eta);
  int maxiter = 20000;
  Vector energy_values(T);
  Vector z_values(T);
  Vector overlaps(T);
  energy_values.setZero();
  z_values.setZero();
  overlaps.setZero();

  std::string sigma_float = std::to_string(sigma);
  sigma_float.erase( sigma_float.find_last_not_of('0') + 1, std::string::npos);
  
  // Create folders and stuff
  std::string foldername = "sigma_" + sigma_float + "/";

  // create folder
  int status = mkdir(foldername.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  std::string to_append = test.underscore + std::to_string((int)alpha) + test.underscore + std::to_string(N) + test.underscore + std::to_string(T) + ".dat";
  // create files
  std::string prices_file = "prices" + to_append;
  std::string xi_file = "xi_matrix" + to_append;
  std::string rar_file = "rar" + to_append;

  long rows_p = N;
  long cols_p = 1;

  long rows_xi = test.getM();
  long cols_xi = N;
  std::ofstream prices_output(foldername+prices_file, std::ios::out | std::ios::binary | std::ios::trunc);
  
  std::ofstream xi_output(foldername+xi_file, std::ios::out | std::ios::binary | std::ios::trunc);
  
  std::ofstream rar_output(foldername+rar_file);
  assert(prices_output.is_open());
  assert(xi_output.is_open());
  assert(rar_output.is_open());

  prices_output.write( (char *) (&rows_p), sizeof(rows_p));
  prices_output.write( (char *) (&cols_p), sizeof(cols_p));
  prices_output.write( (char *) test.prices.data(), rows_p*cols_p*sizeof(typename Eigen::MatrixXd::Scalar));

  xi_output.write( (char *) (&rows_xi), sizeof(rows_xi));
  xi_output.write( (char *) (&cols_xi), sizeof(cols_xi));
  xi_output.write( (char *) test.xi_matrix.data(), rows_xi*cols_xi*sizeof(typename Eigen::MatrixXd::Scalar));


  double overlap = 0.0;
  Vector prices_copy = test.prices;
  for (int i = 0; i< T; i++){
    std::cout << "Running iteration number: " << i+1 << std::endl;
    test.run_gd(maxiter, 1000, true);
    energy_values(i) = test.energy;
    z_values(i) = test.z;
    // prices_output << test.prices.transpose() << std::endl;
    prices_output.write( (char *) test.prices.data(), rows_p*cols_p*sizeof(typename Eigen::MatrixXd::Scalar));
    // xi_output << test.xi_matrix << std::endl;
    xi_output.write( (char *) test.xi_matrix.data(), rows_xi*cols_xi*sizeof(typename Eigen::MatrixXd::Scalar));
    std::cout << "Replacing constraints " << std::endl;
    overlap = test.prices.transpose() * prices_copy;
    overlap = overlap/N;
    overlaps(i) = overlap;
    std::cout << "Z = " << test.z << " Energy = " << test.energy << " Overlap = " << overlap << std::endl;
    test.replace_violated_constraints();
  }
  // std::string filename = test.replacement_filename;
  // filename = filename + test.underscore  + std::to_string(T) + ".dat";
  // std::cout << "Writing data to file" << std::endl;
  // std::ofstream write_output(filename);
  // assert(write_output.is_open());
  prices_output.close();
  xi_output.close();
  
  for (int i = 0; i< T; i++){
    double c = z_values(i)*1.0/test.getM();
    rar_output << i+1 << "\t" << z_values(i) << "\t" << c << "\t" << energy_values(i)  << "\t" << overlaps(i) << std::endl;
  }
  rar_output.close();  		
}


