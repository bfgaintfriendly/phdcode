#include "perceptron.hpp"
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>

int main(int argc, char *argv[]){

  std::vector <std::string> arguments;

  for (int i =1; i < argc; i++){
    arguments.push_back(argv[i]);
  }

  std::string foldername = arguments[0];
  double alpha = std::atof(argv[2]);
  double sigma = std::atof(argv[3]);
  double pbar = std::atof(argv[4]);
  double Delta = std::atof(argv[5]); 
  int N = std::atoi(argv[6]);
  // int T = atoi(argv[2]);
  int T = std::atoi(argv[7]);
  int interval = std::atoi(argv[8]);
  int verbose = 0;
  double eta = 0.0;
  Perceptron test(alpha, N, sigma, Delta, pbar, eta);

  int maxiter = 20000;

  Vector energy_values(T);
  Vector z_values(T);
  Vector overlaps(T);

  energy_values.setZero();
  z_values.setZero();
  overlaps.setZero();

   std::string sigma_float = std::to_string(sigma);
  sigma_float.erase( sigma_float.find_last_not_of('0') + 1, std::string::npos);

  std::string alpha_float = std::to_string(alpha);
  alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

  // Create folders and stuff
  foldername = foldername + "/";
  // create folder
  // int status = mkdir(foldername.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  std::string to_append = test.underscore + alpha_float + test.underscore + std::to_string(N) + test.underscore + std::to_string(T) + ".dat";

  std::string filename = "sigma" + test.underscore + sigma_float + to_append;
  std::string filename2 = "prices_output" + test.underscore + sigma_float + to_append;
  // std::string filename3 = "xi_matrix" + test.underscore + sigma_float + to_append;


  std::cout << "-----Checking variables ----- " << std::endl;
  std::cout << "Foldername " << foldername << std::endl;
  std::cout << "Alpha = " << alpha << std::endl;
  std::cout << "Sigma = " << sigma << std::endl;
  std::cout << "pbar = " << pbar << std::endl;
  std::cout << "Delta = " << Delta << std::endl;
  std::cout << "m = " << test.getm() << std::endl;
  std::cout << "N = " << N << std::endl;
  std::cout << "T = " << T << std::endl;
  std::ofstream rar_output(filename);
  std::ofstream prices_output(filename2, std::ios::out | std::ios::binary | std::ios::trunc);
  // std::ofstream xi_output(filename3, std::ios::out | std::ios::binary | std::ios::trunc);
  assert(prices_output.is_open());
  assert(rar_output.is_open());
  //  assert(xi_output.is_open());
  long rows_p = test.prices.rows();
  long cols_p = test.prices.cols();
  long size_p = rows_p*cols_p;
  prices_output.write( (char *) (&rows_p), sizeof(rows_p));
  prices_output.write( (char *) (&cols_p), sizeof(cols_p));
  prices_output.write( (char *) test.prices.data(), size_p*sizeof(typename Eigen::MatrixXd::Scalar));

  //  long rows_xi = test.getM();
  //  long cols_xi = N;
  //  long size_xi = rows_xi*cols_xi;
  //  xi_output.write( (char *) (&rows_xi), sizeof(rows_xi));
  //  xi_output.write( (char *) (&cols_xi), sizeof(cols_xi));
  //  xi_output.write( (char *) test.xi_matrix.data(), size_xi*sizeof(typename Eigen::MatrixXd::Scalar));

  Vector prices_copy = test.prices;
  bool to_print = false;
  if (verbose == 1){
    to_print = true;
  }
  double overlap = 0.0;
  for (int i = 0; i < T; i++){
    std::cout << "Running iteration number = " << i+1 << std::endl;

    test.run_gd(maxiter, 1000,to_print);
    energy_values(i) = test.energy;
    z_values(i) = test.z;
    prices_output.write( (char *) test.prices.data(), size_p*sizeof(typename Eigen::MatrixXd::Scalar));
    overlap = test.prices.transpose()*prices_copy;
    overlap = overlap/N;
    overlaps(i) = overlap;
    double c = z_values(i)*1.0/test.getM();
    rar_output << i+1 << "\t" << z_values(i) << "\t" << c << "\t" << energy_values(i)  << "\t" << overlaps(i) << std::endl;
    std::cout << "Z = " << test.z*1.0/test.getM() << " Energy = " << test.energy << " Overlap = "<< overlap << std::endl;

    if (test.energy == 0){
      std::cout << "Energy reached zero. Stopping simulation" << std::endl;
      //      if (i%interval == 0 && i>interval){
      //        xi_output.write( (char *) test.xi_matrix.data(), size_xi*sizeof(typename Eigen::MatrixXd::Scalar));
      //      }
      break;
    }

    // if (i < interval){
    //   xi_output.write( (char *) test.xi_matrix.data(), size_xi*sizeof(typename Eigen::MatrixXd::Scalar));
    // }
    // if (i%interval == 0 && i>interval){
    //   xi_output.write( (char *) test.xi_matrix.data(), size_xi*sizeof(typename Eigen::MatrixXd::Scalar));
    // }
    std::cout << "Replacing constraints "  << std::endl;
    std::cout << " -----------------------------" << std::endl;
    test.replace_violated_constraints();
  }

  prices_output.close();
  // xi_output.close();
  
  // for (int i = 0; i< T; i++){
  //   double c = z_values(i)*1.0/test.getM();
  //   rar_output << i+1 << "\t" << z_values(i) << "\t" << c << "\t" << energy_values(i)  << "\t" << overlaps(i) << std::endl;
  // }
  rar_output.close();

} 
