#include "perceptron.hpp"
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>

int main(int argc, char *argv[]){

  // What I want to do is to run a simulation multiple times for sigma=-1.5 and compute the overlap between price vector n and price vector n+1.      Actually, this is not the way to do it. I create a Perceptron object, run the simulation for the same disorder i.e. the same demand vectors.

  std::vector <std::string> arguments;

  for (int i =1; i < argc; i++){
    arguments.push_back(argv[i]);
  }
  std::string foldername = arguments[0];
  double alpha = std::atof(argv[2]);
  double sigma = std::atof(argv[3]);
  double pbar = std::atof(argv[4]);
  double Delta = std::atof(argv[5]); 
  int N = std::atoi(argv[6]);
  // int T = atoi(argv[2]);
  int T = std::atoi(argv[7]);
  int nb_runs = std::atoi(argv[8]);
  //Number of times I will run the procedure.
  double eta = 0.0;

  bool to_print = false;
  int maxiter = 20000;
  Vector overlaps(nb_runs);
  overlaps.setZero();
  double overlap = 0.0;

  const double eps = 1e-9;


  std::string sigma_float = std::to_string(sigma);
  sigma_float.erase( sigma_float.find_last_not_of('0') + 1, std::string::npos);

  std::string alpha_float = std::to_string(alpha);
  alpha_float.erase( alpha_float.find_last_not_of('0') + 1, std::string::npos);

  std::string underscore = std::string("_");
  foldername = foldername + "/";
  // create folder
  // int status = mkdir(foldername.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  std::string to_append = underscore + alpha_float + underscore + std::to_string(N) + underscore + std::to_string(T) + underscore + std::to_string(nb_runs) + ".dat";

  std::string filename = "sigma" + underscore + sigma_float + to_append;

  std::ofstream overlap_output(filename);
  assert(overlap_output.is_open());



  // This is what will happen for a particular run
  // 1. First we make the object
  // 2. Then we copy the prices and demands into auxiliary variables.
  // 3. Run the gradient descent and the R&R procedure
  // 4. Store final result of demands and prices into xi_{1} and p_{1}
  // 5. Replace the test.prices and test.xi_matrix by the initial copies of the prices and demands.
  // 6. Run the GD and R&R process again.

  // Actually, it could be the case that the result after the first pass of the GD is the same for both initial conditions. However, lets run them again anyway.
  for (int i = 0; i < nb_runs; i++){

    std::cout << "---- Running run number " << i+1 << " -------" << std::endl;
    Perceptron test(alpha, N, sigma, Delta, pbar, eta);

    Vector prices_copy = test.prices;
    Matrix xi_matrix_copy = test.xi_matrix;

    Vector p_1(N);
    Matrix xi_1(test.getM(), N);
    p_1.setZero();
    xi_1.setZero();

    for (int k = 0; k < 2; k++){
  //One pass of the R&R procedure
      std::cout << "Pass number of R&R procedure " << k+1 << std::endl;
      for (int j = 0; j < T; j++){
        test.run_gd(maxiter, 1000, to_print);

        if (test.energy < eps){
          std::cout << "Energy reached zero. Stopping R&R procedure" << std::endl;
          break;
        }

        test.replace_violated_constraints();
      }

      overlap = p_1.transpose()*test.prices;
      overlap = overlap/N;

      std::cout << "Overlap " << overlap << std::endl;

      p_1 = test.prices;
      xi_1 = test.xi_matrix;


      test.prices = prices_copy;
      test.xi_matrix = xi_matrix_copy;
    }

    overlap_output << i+1 << "\t" << overlap << std::endl;
    overlap = 0.0;
  }

  overlap_output.close();

}
