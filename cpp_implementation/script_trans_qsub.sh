copy=$1
N=$2
sigma=$3
f="run"
u="_"
ofile=$f$u$N$u$sigma$u$copy
g++ -I ~/installs/eigen -std=c++11 transition_point.cpp -o experiments/transition_point/$ofile -O2 -march=native

cd experiments/transition_point
./$ofile $copy $N $sigma
