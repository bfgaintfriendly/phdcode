T=1000
pbar=1.5
Delta=4.5
interval=50

N=100
alpha=10
for sigma in -1.5 -1.0 -0.52 -0.5 0.0 0.5 1.0 1.5
do
    echo "Running with sigma = $sigma"
    ./one_script_to_rule_them_all.sh alpha_fixed_sigma_1 $alpha $sigma $pbar $Delta $N $T $interval
done

alpha=20
for sigma in -1.5 -1.0 -0.52 -0.5 0.0 0.5 1.0 1.5
do
    echo "Running with sigma = $sigma"
    ./one_script_to_rule_them_all.sh alpha_fixed_sigma_2 $alpha $sigma $pbar $Delta $N $T $interval
done

alpha=30
for sigma in -1.5 -1.0 -0.52 -0.5 0.0 0.5 1.0 1.5
do
    echo "Running with sigma = $sigma"
    ./one_script_to_rule_them_all.sh alpha_fixed_sigma_3 $alpha $sigma $pbar $Delta $N $T $interval
done


alpha=40
for sigma in -1.5 -1.0 -0.52 -0.5 0.0 0.5 1.0 1.5
do
    echo "Running with sigma = $sigma"
    ./one_script_to_rule_them_all.sh alpha_fixed_sigma_4 $alpha $sigma $pbar $Delta $N $T $interval
done


alpha=50
for sigma in -1.5 -1.0 -0.52 -0.5 0.0 0.5 1.0 1.5
do
    echo "Running with sigma = $sigma"
    ./one_script_to_rule_them_all.sh alpha_fixed_sigma_5 $alpha $sigma $pbar $Delta $N $T $interval
done

alpha=60
for sigma in -1.5 -1.0 -0.52 -0.5 0.0 0.5 1.0 1.5
do
    echo "Running with sigma = $sigma"
    ./one_script_to_rule_them_all.sh alpha_fixed_sigma_6 $alpha $sigma $pbar $Delta $N $T $interval
done

alpha=70
for sigma in -1.5 -1.0 -0.52 -0.5 0.0 0.5 1.0 1.5
do
    echo "Running with sigma = $sigma"
    ./one_script_to_rule_them_all.sh alpha_fixed_sigma_7 $alpha $sigma $pbar $Delta $N $T $interval
done
