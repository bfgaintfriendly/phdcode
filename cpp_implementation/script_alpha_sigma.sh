# T=50
# echo "Running with T = 50"
# for i in -1.5 -1.0 -0.5 0.0 0.5 1.0 1.5
# do
#     echo "Running with sigma = $i"
#     ./alpha_vs_sigma $i $T
#     command; echo "Run completed" | mail -s "Progress for alpha_vs_sigma. Sigma = $i" sharma@lpt.ens.fr
# done
T=100
echo "Running with T = $T"
for i in -0.52 -0.5
do
    echo "Running with sigma = $i"
    ./alpha_vs_sigma $i $T 0
    command; echo "Run completed"| mail -s "Progress for alpha_vs_sigma. Sigma = $i" sharma@lpt.ens.fr
done    
