T=$1
sigma=-1.5
echo "Running with sigma = $sigma"
for i in 10 20 30 40 50 60 70 80 90 100
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 100 $T
    command; echo "Alpha = $i done"| mail -s "Progress alpha = $i. Sigma $sigma" sharma@lpt.ens.fr
done
sigma=-0.5
echo "Running with sigma = $sigma"
for i in 3 4 5
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 500 $T
    command; echo "Alpha = $i done" | mail -s "Progress alpha = $i. Sigma $sigma" sharma@lpt.ens.fr
done
for i in 10 20 30 
do
    echo "Running with alpha = $i"
    ./repeal_and_replace $i $sigma 100 $T
    command; echo "Alpha = $i done" | mail -s "Progress alpha = $i. Sigma $sigma" sharma@lpt.ens.fr
done
sigma=0.5
echo "Running with sigma = $sigma"
./repeal_and_replace 1.0 $sigma 500 $T
command; echo "Alpha = 1.0 done" | mail -s "Progress alpha = 1.0. Sigma $sigma" sharma@lpt.ens.fr
./repeal_and_replace 10.0 $sigma 100 $T
command; echo "Alpha = 10.0 done" | mail -s "Progress alpha = 10.0. Sigma $sigma" sharma@lpt.ens.fr
