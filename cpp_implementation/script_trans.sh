sigmas=(-1.0 -0.52 -0.5 1.0)
Ns=(50 100 200 300 400 500 1000)
for sigma in ${sigmas[*]}
do
    for N in ${Ns[*]}
    do
        for copy in {1..10}
        do
            qsub -cwd -m e -b y ./script_trans_qsub.sh $copy $N $sigma
        done
    done
done
# qsub -cwd -b y ./script_trans_qsub.sh 0 100 1.5


