#include "../perceptron.hpp"

int main(int argc, char *argv[]){
  double alpha = 20.0;

  int N = 200;

  double eta = 0.0;

  double Delta = 4.5;
  int maxiter = 20000;

  //User defined variables

  double pbar = std::atof(argv[1]);
  double eps_p = std::atof(argv[2]);
  double eps_d;
  eps_d = eps_p;

  double sigma = std::atof(argv[3]);
  int copy = std::atoi(argv[4]);

  double omega = std::atof(argv[5]);

  Perceptron object(alpha, N, sigma, Delta, pbar, eta, eps_p, eps_d);

  int T = 500;
  bool hard = true;
  object.dynamics_ema(T,omega,copy,hard);
}
