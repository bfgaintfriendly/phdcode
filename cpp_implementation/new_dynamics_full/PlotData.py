from evals_utils import *
import glob 
import os

class PlotData():
    def __init__(self, path_to_data, sigmas, n_eps = 4, n_ema = 2):
        self.path_data = path_to_data
        self.sigmas = sigmas
        self.n_eps = n_eps
        self.n_ema = n_ema
        self.path_evals_a = self.path_data + 'evals_a_'
        self.path_evals_b = self.path_data + 'evals_b_'
        self.evals_a_files = [os.path.basename(name) for name in glob.glob(self.path_evals_a + '*')]
        self.evals_b_files = [os.path.basename(name) for name in glob.glob(self.path_evals_b + '*')]
        
        self.evals_a_files = sorted(self.evals_a_files, key = lambda x: float(x.split('_')[2]))
        self.evals_b_files = sorted(self.evals_b_files, key = lambda x: float(x.split('_')[2]))
        
        self.path_rar = self.path_data + 'rar_avgs_'
        self.path_en = self.path_data + 'en_avgs_'
        self.path_ema = self.path_data +  'ema_avgs_'
        
        self.rar_files = [os.path.basename(name) for name in glob.glob(self.path_rar + '*')]
        self.en_files = [os.path.basename(name) for name in glob.glob(self.path_en + '*')]
        self.ema_files = [os.path.basename(name) for name in glob.glob(self.path_ema+ '*')]
        
        self.rar_files = sorted(self.rar_files, key = lambda x: float(x.split('_')[2]))
        self.en_files = sorted(self.en_files,  key = lambda x: float(x.split('_')[2]))
        self.ema_files = sorted(self.ema_files,  key = lambda x: float(x.split('_')[2]))
        
        
    def plot_evals(self,title, name, omega = 0.2, after=True):
        # nrows, ncols, path, file_list
        if after:
            file_list = self.evals_a_files
        else:
            file_list = self.evals_b_files
        x, rho = mplaw(20.0)
        nrows = len(self.sigmas)
        ncols = self.n_eps
        fig, axes = plt.subplots(nrows, ncols, figsize=(30,15), dpi=400)
        if omega == 0.2:
            file_list = sorted(file_list[:len(self.sigmas)], key = lambda x: float(x.split('_')[3]))
        else:
            file_list = sorted(file_list[len(self.sigmas):], key = lambda x: float(x.split('_')[3])) 
        for i in range(nrows):
            fname = self.path_data+file_list[i]
            all_evals = np.loadtxt(fname)
            print fname
            for j in range(ncols):
                axes[i,j].hist(all_evals[j], density = True, ec = "k", bins=20)
                axes[i,j].plot(x,rho, 'r')
                axes[i,j].xaxis.set_tick_params(labelsize=14)
                axes[i,j].yaxis.set_tick_params(labelsize=14)
        rows = ['$\sigma = ${}'.format(sigma) for sigma in reversed(self.sigmas)]
        cols = ['$\epsilon_p$ = {}'.format(row) for row in [0.01, 0.02, 0.05, 0.1]]
        for ax, col in zip(axes[0], cols):
            ax.set_title(col, size = 'large')
        pad = 5
        for ax, row in zip(axes[:,0], rows):
            ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size='large', ha='right', va='center')

        fig.suptitle(title, fontsize = 30)
        plt.show()
        if name is not None:
            fig.savefig(self.path_data + name+'.png')
            fig.savefig(self.path_data + name+'.pdf')
       
    
    
    def plot_rar(self, title, name, omega = 0.2):
        out_name = 'grid_z_' + str(omega) + '_'
        nrows = len(self.sigmas)
        ncols = self.n_eps
        
        fig, axes = plt.subplots(nrows, ncols, figsize = (30,15), dpi = 400) # for rar_
#         fig2, axes2 = plt.subplot(nrows, ncols, figsize = (30,15), dpi = 400) # for en
#         fig3, axes3 = plt.subplot(nrows, ncols, figsize = (30,15), dpi = 400) # for ema
        
        if omega == 0.2:
            file_list_rar = sorted(self.rar_files[:len(self.sigmas)], key = lambda x: float(x.split('_')[3]))
            file_list_en = sorted(self.en_files[:len(self.sigmas)], key = lambda x: float(x.split('_')[3]))
            file_list_ema = sorted(self.ema_files[:len(self.sigmas)], key = lambda x: float(x.split('_')[3]))
        else:
            file_list_rar = sorted(self.rar_files[len(self.sigmas):], key = lambda x: float(x.split('_')[3]))
            file_list_en = sorted(self.en_files[len(self.sigmas):], key = lambda x: float(x.split('_')[3]))
            file_list_ema = sorted(self.ema_files[len(self.sigmas):], key = lambda x: float(x.split('_')[3]))
        for i in range(nrows):
            fname_rar = self.path_data + file_list_rar[i]
            fname_en = self.path_data + file_list_en[i]
            fname_ema = self.path_data + file_list_ema[i]
            temp1 = np.loadtxt(fname_rar)
            temp2 = np.loadtxt(fname_en)
            temp3 = np.loadtxt(fname_ema)
            
            print fname_rar
            
            for j in range(ncols):
                axes[i,j].plot(temp1[j])
                axes[i,j].xaxis.set_tick_params(labelsize=14)
                axes[i,j].yaxis.set_tick_params(labelsize=14)
        rows = ['$\sigma = ${}'.format(sigma) for sigma in reversed(self.sigmas)]
        cols = ['$\epsilon_p$ = {}'.format(row) for row in [0.01, 0.02, 0.05, 0.1]]
        for ax, col in zip(axes[0], cols):
            ax.set_title(col, size = 'large')
        pad = 5
        for ax, row in zip(axes[:,0], rows):
            ax.annotate(row, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),
                    xycoords=ax.yaxis.label, textcoords='offset points',
                    size='large', ha='right', va='center')

        fig.suptitle(title, fontsize = 30)
    #     fig.tight_layout()
        plt.show()
        print out_name + name
        if name is not None:
            fig.savefig(self.path_data + out_name + name+'.png')
            fig.savefig(self.path_data + out_name + name+'.pdf')
            
       
        