
import numpy as np
import glob 
import os 
import scipy

from evals_utils import *

sigmas_1 = [0.5, 0.25,0.0, -0.25, -0.5, -0.6, -1.0]
sigmas_0 = [0.5, 0.25, 0.0, -0.25, -0.5]
class GenerateData():
    
    def __init__(self, path_to_data, sigmas, n_eps=4, n_ema=2):
        self.path_data = path_to_data
        self.path_rar = self.path_data + 'rr_data/'
        self.path_xi = self.path_data + 'xis/'
        self.path_prices = self.path_data + 'prix/'
        self.sigmas = sigmas
        self.n_eps = n_eps
        self.n_ema = n_ema
        
        self.rar_files = [os.path.basename(x) for x in glob.glob(self.path_rar + '*')]
        self.xi_files = [os.path.basename(x) for x in glob.glob(self.path_xi + '*')]
        self.price_files = [os.path.basename(x) for x in glob.glob(self.path_prices + '*')]
        
    def sort_files_sigma_xi(self):
        sorted_files = []
        for sigma in self.sigmas:
            sorted_files.append(sorted([name for name in self.xi_files if (float(name.split('_')[2]) == sigma)]))
        return sorted_files
    
    def sort_files_omega_xi(self, omega = 0.2):
        sorted_files = self.sort_files_sigma_xi()
        for i in range(len(self.sigmas)):
            temp = sorted_files[i]
            temp = sorted(temp, key=lambda x: float(x.split('_')[6]))
            sorted_files[i] = temp
        return sorted_files        
        
    def generate_rar_avgs(self, omega = 0.2):
        rar_filename = 'rar_avgs_'
        en_filename = 'en_avgs_'
        ema_filename = 'ema_avgs_'
        rar_filename = rar_filename + str(omega) + '_'
        en_filename = en_filename + str(omega) + '_'
        ema_filename = ema_filename + str(omega) +  '_'
        print rar_filename, en_filename, ema_filename
        for sigma in self.sigmas:
            print sigma
            rel_files_rar = sorted([name for name in self.rar_files if (float(name.split('_')[1]) == sigma)])
            rar_avgs = np.zeros((self.n_eps,500))
            en_avgs = np.zeros((self.n_eps,500))
            ema_avgs = np.zeros((self.n_eps,500))
            rar_files_sorted = sorted(rel_files_rar, key=lambda x: float(x.split('_')[5]))
            if omega == 0.2:
                rar_files_sorted = rar_files_sorted[:8]
            else:
                rar_files_sorted = rar_files_sorted[8:]
            for i in range(self.n_eps):
                for j in range(i*self.n_ema, (i+1)*self.n_ema):
                    filename = self.path_rar+rar_files_sorted[j]
                    temp = np.loadtxt(filename, skiprows=1)
                    rar_avgs[i] += temp[:,2][:]
                    en_avgs[i] += temp[:,3][:]
                    ema_avgs[i] += temp[:,5][:]

                rar_avgs[i] /= self.n_ema
                en_avgs[i] /= self.n_ema
                ema_avgs[i] /= self.n_ema
            np.savetxt(self.path_data+rar_filename+ str(sigma), rar_avgs)
            np.savetxt(self.path_data+en_filename+ str(sigma), en_avgs)
            np.savetxt(self.path_data + ema_filename + str(sigma), ema_avgs)
        
        
          
    def generate_evals(self, omega = 0.2):
        eb_filename = 'evals_b_' + str(omega) + '_'
        ea_filename = 'evals_a_' + str(omega) + '_'
        print ea_filename, eb_filename
        for sigma in self.sigmas:
            print sigma
            rel_files_xi = sorted([name for name in self.xi_files if (float(name.split('_')[2]) == sigma)])
            evals_b = []
            evals_a = []
            xi_files_sorted = sorted(rel_files_xi, key=lambda x: float(x.split('_')[6]))
            if omega == 0.2:
                xi_files_sorted = xi_files_sorted[:8]
            else:
                xi_files_sorted = xi_files_sorted[8:]
            for i in range(self.n_eps):
                temp_b = np.asarray([])
                temp_a = np.asarray([])
                for j in range(i*self.n_ema, (i+1)*self.n_ema):
                    filename = self.path_xi + xi_files_sorted[j]
                    xi_data = np.loadtxt(filename)
                    xi_before = xi_data[4000:8000]
                    xi_after = xi_data[8000:]
                    evals_before = compute_evals(normalize_matrix(xi_before.T), 4000)
                    evals_after = compute_evals(normalize_matrix(xi_after.T), 4000)
                    temp_b = np.concatenate([temp_b, evals_before])
                    temp_a = np.concatenate([temp_a, evals_after])
                evals_b.append(temp_b)
                evals_a.append(temp_a)
            np.savetxt(self.path_data+ eb_filename + str(sigma), np.asarray(evals_b))
            np.savetxt(self.path_data+ ea_filename + str(sigma), np.asarray(evals_a))

        
    
