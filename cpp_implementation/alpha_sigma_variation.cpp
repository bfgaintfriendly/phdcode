#include "perceptron.hpp"
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>

int main(int argc, char *argv[]){

  double alpha = 100.0;
  int N = 100;
  // int T = atoi(argv[2]);
  int T = 1000;
  int verbose = atoi(argv[1]);
  
  double Delta = 4.5;
  double pbar = 1.5;
  double eta = 0.0;
  // double sigma = atof(argv[1]);
  double sigma = -0.5;
  Perceptron test(alpha, N, sigma, Delta, pbar, eta);

  int maxiter = 20000;

  Vector energy_values(T);
  Vector z_values(T);
  Vector overlaps(T);

  energy_values.setZero();
  z_values.setZero();
  overlaps.setZero();

  

  std::string sigma_float = std::to_string(sigma);
  sigma_float.erase( sigma_float.find_last_not_of('0') + 1, std::string::npos);
  
  // Create folders and stuff
  std::string foldername = "alpha_fixed_sigma_variation_longer_T";
  foldername = foldername + "/";
  // create folder
  int status = mkdir(foldername.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  std::string to_append = test.underscore + std::to_string((int)alpha) + test.underscore + std::to_string(N) + test.underscore + std::to_string(T) + ".dat";

  std::string filename = "sigma" + test.underscore + sigma_float + to_append;
  std::string filename2 = "prices_output" + test.underscore + sigma_float + to_append; 
  std::ofstream rar_output(foldername+filename);
  std::ofstream prices_output(foldername+filename2, std::ios::out | std::ios::binary | std::ios::trunc);
  assert(prices_output.is_open());
  assert(rar_output.is_open());

  long rows_p = test.prices.rows();
  long cols_p = test.prices.cols();
  long size_p = rows_p*cols_p;
  prices_output.write( (char *) (&rows_p), sizeof(rows_p));
  prices_output.write( (char *) (&cols_p), sizeof(cols_p));
  prices_output.write( (char *) test.prices.data(), size_p*sizeof(typename Eigen::MatrixXd::Scalar));
  
  Vector prices_copy = test.prices;
  bool to_print = false;
  if (verbose == 1){
    to_print = true;
  }
  double overlap = 0.0;
  for (int i = 0; i < T; i++){
    std::cout << "Running iteration number = " << i+1 << std::endl;
      
    test.run_gd(maxiter, 1000,to_print);
    energy_values(i) = test.energy;
    z_values(i) = test.z;
    prices_output.write( (char *) test.prices.data(), size_p*sizeof(typename Eigen::MatrixXd::Scalar));
    overlap = test.prices.transpose()*prices_copy;
    overlap = overlap/N;
    overlaps(i) = overlap;
    std::cout << "Z = " << test.z*1.0/test.getM() << " Energy = " << test.energy << " Overlap = "<< overlap << std::endl;
    std::cout << "Replacing constraints "  << std::endl;
    std::cout << " -----------------------------" << std::endl;
    test.replace_violated_constraints();
  }

  prices_output.close();
  
  for (int i = 0; i< T; i++){
    double c = z_values(i)*1.0/test.getM();
    rar_output << i+1 << "\t" << z_values(i) << "\t" << c << "\t" << energy_values(i)  << "\t" << overlaps(i) << std::endl;
  }
  rar_output.close();

} 
  
// }
