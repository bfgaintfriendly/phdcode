#include "perceptron.hpp"

#include <cstring>

int main(int argc, char *argv[]){

  std::string foldername = "test";
  foldername = foldername + "/";

  double sigma = -1.0;
  int T = 10;

  std::string filename = "xi_matrix_-1.5_10_100_10.dat";

  std::cout << "Sigma = " << sigma << std::endl;
  std::cout << filename << std::endl;
  std::ifstream in (filename, std::ios::in | std::ios::binary);

  // Checking file size 
  in.seekg(0,std::ios::end);
  int length_file = (int) in.tellg();

  std::cout << "Length of file " << length_file << std::endl;

  in.seekg(0, std::ios::beg);
  int N = 100;

  long rows = 0;
  long cols = 0;

  in.read( (char *) (&rows), sizeof(rows));
  in.read( (char *) (&cols), sizeof(cols));

  int stop_time = length_file/(sizeof(double)*rows*cols);
  std::cout << "Simulation stopped at "  << stop_time << std::endl;
  Matrix prices;
  prices.resize(rows, cols);
  in.read( (char *) prices.data(), rows*cols*sizeof(typename Eigen::MatrixXd::Scalar));
 
  std::cout << "Rows = " << rows << " Cols = " << cols << std::endl;
  std::cout << "Initial Prices = " << std::endl;
  //std::cout << prices.transpose() << std::endl;
  for (int i = 0; i<stop_time ; i++){
    std::cout << "------- Iteration number ----- " << i << std::endl;
    in.read( (char *) prices.data(), rows*cols*sizeof(typename Eigen::MatrixXd::Scalar));
    // std::cout << prices.transpose() << std::endl;
  }

  in.close();
}
