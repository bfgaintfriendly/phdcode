#include "../perceptron.hpp"

int main(int argc, char *argv[]){
  double alpha = 20.0;
  //int N = std::atoi(argv[1]);
  int N = 200;
  double eta = 0.0;
  //double pbar = 1.5;
  double Delta = 4.5;
  int maxiter = 20000;

  //Parameters from the user
  double pbar = std::atof(argv[1]);
  double eps_p = std::atof(argv[2]);
  double eps_d;
  eps_d  = eps_p;
  double sigma = std::atof(argv[3]);
  int copy = std::atoi(argv[4]);
  
  Perceptron object(alpha, N, sigma, Delta, pbar, eta, eps_p, eps_d);
  std::cout << "eta " <<  object.geteta() << " eps_d " << object.getepsp() << " eps_d " << object.getepsd() <<  std::endl;

  int T = 500;

  object.dynamics_new(T,copy);

}
