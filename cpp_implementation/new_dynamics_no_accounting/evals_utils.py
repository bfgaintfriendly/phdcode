import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import norm


def get_ipr(vector):
    return np.sum(np.power(vector,4)) 

def gen_gaussian(mu=0, sigma = 1, level=4):
    x = np.linspace(mu-4*sigma, mu+4*sigma, 100)
    rv = norm()
    return x, rv.pdf(x)

def phist(data, density = True, bins = 15, style = None):
    if style is None:
        plt.hist(data, ec ="k", density=density, bins =bins)
    else:
        plt.hist(data,ec = "k", density = density, bins = bins, histtype=u'step')
    plt.yticks(fontsize=14)
    plt.xticks(fontsize=14)

def mplaw(alpha, var = 1.0):
    eps = 1e-4
    q = 1./alpha
    l_plus = var*(np.sqrt(q)+1)**2 
    l_minus = var*(1-np.sqrt(q))**2 
    l_range = np.arange(l_minus, l_plus+eps, eps)
    mp_vals = []
    for l in l_range:
        ##num = np.sqrt(4*l*q- (l-1+q)**2)
        num = np.sqrt((l_plus-l)*(l-l_minus))
        den = 2*np.pi*(l*q)*var
        mp_vals.append(num/den)
    mp_vals = np.asarray(mp_vals)
    
    # to take into account the transposed case 
    # formula taken from https://arxiv.org/pdf/0910.1205.pdf page 10
    if q > 1:
        mp_vals = mp_vals * q
    return l_range, mp_vals


def normalize_matrix(matrix):
    test = np.copy(matrix)
    for i in range(test.shape[0]):
        mean = np.average(test[i])
        #var = np.average(np.square(test[i]))
        var = np.std(test[i])
        test[i] = test[i] - mean
        test[i] = test[i]/var
    return test


def compute_evals(matrix, div):
    c = matrix.dot(matrix.T)
    # print matrix.shape[0]
    # c = c/matrix.shape[1]
    c = c/div
    w = np.linalg.eigvalsh(c)
    return w



def compute_evecs(matrix, div):
    c = matrix.dot(matrix.T)
    # c = c/matrix.shape[1]
    c = c/div
    w,v = np.linalg.eigh(c)
    return w,v

def compute_eigenvalues(fname):
    evals_raw_b = np.asarray([])
    evals_norm_b = np.asarray([])
    evals_raw_a = np.asarray([])
    evals_norm_a = np.asarray([])

#         fname = 'onewgd_m0/xi_data_1._20._200_gd'
#     print "Running for filename : {}".format(fname+str(i))
    xi = np.loadtxt(fname)
    xi_before = xi[:4000]
    xi_after = xi[-1*4000:]
    evals_raw_b = np.concatenate([evals_raw_b, compute_evals(xi_before)])
    evals_norm_b = np.concatenate([evals_norm_b, compute_evals(normalize_matrix(xi_before))])
    evals_raw_a = np.concatenate([evals_raw_a, compute_evals(xi_after)])
    evals_norm_a = np.concatenate([evals_norm_a, compute_evals(normalize_matrix(xi_after))])
    print "Size of evals_norm_a: {}".format(evals_norm_a.shape)
    return evals_raw_b, evals_norm_b, evals_raw_a, evals_norm_a


def plot_eigenvalues(evals_raw_b, evals_norm_b, evals_raw_a, evals_norm_a, sigma, pbar, save_name):
    plt.figure(figsize = (20,20), dpi = 200)
    l,m = mplaw(20)
    plt.subplot(221)
    plt.hist(evals_raw_b,density = True,bins=15, ec="k")
    plt.plot(l,m, 'r')
    plt.yticks(fontsize=14)
    plt.xticks(fontsize=14)
    plt.ylabel( '$\\rho (\\lambda)$', fontsize=14)
    plt.xlabel('$\\lambda$', fontsize = 14)
    plt.title('Un-normalized (Before)', fontsize = 18)
    plt.legend()
    plt.subplot(222)
    plt.hist(evals_norm_b, density=True ,bins=15, ec="k")
    plt.plot(l,m, 'r')
    plt.yticks(fontsize=14)
    plt.xticks(fontsize=14)
    plt.ylabel( '$\\rho (\\lambda)$', fontsize=14)
    plt.xlabel('$\\lambda$', fontsize = 14)
    plt.title('Normalized (Before)', fontsize = 18)
    plt.legend()
    plt.subplot(223)
    x1, y1, _ = plt.hist(evals_raw_a, density = True,bins = 15, ec="k")
    plt.plot(l,m, 'r')
    plt.yticks(fontsize=14)
    plt.xticks(fontsize=14)
    plt.ylabel( '$\\rho (\\lambda)$', fontsize=14)
    plt.xlabel('$\\lambda$', fontsize = 14)
    plt.title('Un-normalized (After)', fontsize = 18)
    plt.legend()
    plt.subplot(224)
    x2, y2, _ = plt.hist(evals_norm_a,density=True, bins=15, ec="k")
    plt.plot(l,m, 'r')
    plt.yticks(fontsize=14)
    plt.xticks(fontsize=14)
    plt.ylabel( '$\\rho (\\lambda)$', fontsize=14)
    plt.xlabel('$\\lambda$', fontsize = 14)
    plt.title('Normalized (After)', fontsize = 18)
    plt.legend()
    plt.suptitle('$\\alpha = 20.0$, $\\sigma$ = {}, m = {}'.format(sigma, pbar), fontsize = 26)
    plt.savefig(save_name+'.pdf')
    plt.savefig(save_name+'.png')