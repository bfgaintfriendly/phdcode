#include <iostream>
// #include <Eigen/Dense>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <iomanip>
#include <random>
#include <cmath>
#include <iterator>
#include <functional>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstring>
#include <typeinfo>
//#include <Eigen/Eigenvalues> 
#include "../perceptron.hpp"
//#include "../utils.hpp"

int main(int argc, char *argv[]){
  // Eigen::setNbThreads(4);
  // std::cout << "Number of threads used = " << Eigen::nbThreads() << std::endl;

  // std::cout << "Processes = " << omp_get_num_procs() << std::endl;
  double alpha = 20.0;
  //int N = std::atoi(argv[1]);
  int N = 200;
  double eta = 0.0;
  //double pbar = 1.5;
  double Delta = 4.5;
  //int T = 2;
  int nb_copies = 2;
  double sigma = -1.0;
  int nb_runs = 2;
  int maxiter = 20000;

  //Parameters from the user
  double pbar = std::atof(argv[1]);
  double eps_p = std::atof(argv[2]);
  double eps_d;
  eps_d  = eps_p; 
  Perceptron object(alpha, N, sigma, Delta, pbar, eta, eps_p, eps_d);
  std::cout << "eta " <<  object.geteta() << " eps_d " << object.getepsp() << " eps_d " << object.getepsd() <<  std::endl;

  // int rr = std::atoi(argv[1]);
  // bool remove_agents = false;
  // if (rr == 1){
  //   remove_agents = true;
  // }
  // int T = 3;
  // bool remove_agents = true;
  // int copy = 0;
  
  // object.dynamics_new(T, copy);
  // object.run_gd(maxiter);

  // for (int i=0; i< 10; i++){
  //   std::cout << object.generate_delta() << " " ;
  // }
  // std::cout << std::endl;
  // double eps_d = 0.01;

  // double eps_p = 0.01;
  
  // std::cout << "Nb rows " << object.xi_matrix.rows() << " nb cols " << object.xi_matrix.cols() << std::endl;
  // int M = object.getM();

  // //Generating Supply and Demand Vectors;
  // for (int i = 0; i < N; i++){
  //   Vector test = object.xi_matrix.col(i);
  //   //std::cout << "Working with column number " << i << std::endl;
  //   //std::cout << "Size of column " << test.size() << std::endl;
  //   double temp_supply = 0.0;
  //   double temp_demand = 0.0;
  //   for (int j = 0; j  < test.size(); j++){
  //     bool buy_sell = (test(j) > 0);
  //     if (buy_sell){
  // 	temp_supply += test(j);
  //     }
  //     else {
  // 	temp_demand += test(j);
  //     }
  //   }
  //   //std::cout << "Supply " << temp_supply << " Demand" << temp_demand << std::endl;
  //   object.supply(i) = temp_supply;
  //   object.demand(i) = temp_demand;
    
  // }

  // //Updating the demand matrix

  // for (int i = 0; i< M; i++){
  //   for (int j = 0; j < N; j++){
  //     double val = object.xi_matrix(i,j);
  //     if( val  > 0){
  // 	//std::cout << "Agent is a seller" << std::endl;
  // 	if (object.supply(j) > abs(object.demand(j))){
  // 	  //std::cout << "Supply greater than demand " << std::endl;
  // 	  double delta = object.generate_delta();
  // 	  double temp = val*(1+eps_d*delta);
  // 	  object.xi_matrix(i,j) = temp;
  // 	  //std::cout << val <<"\t" << temp << std::endl;
  // 	}
  // 	else{
  // 	  //std::cout << "Demand greater than supply" << std::endl;
  // 	  double delta = object.generate_delta();
  // 	  double temp = val*(1-eps_d*delta);
  // 	  object.xi_matrix(i,j) = temp;
  // 	  //	  std::cout << val <<"\t" << temp << std::endl;
  // 	}
  //     }
  //     else{
  // 	//	std::cout << "Agent is a buyer " << std::endl;
  // 	if (object.prices(j) > object.getm()){
  // 	  //std::cout << "Good too expensive " << std::endl;
  // 	  double delta = object.generate_delta();
  // 	  double temp = val*(1 -eps_p*delta);
  // 	  object.xi_matrix(i,j) = temp;
  // 	  //	  std::cout << val <<"\t" << temp << std::endl;
  // 	}
  // 	else{
  // 	  //	  std::cout << "Good is cheaper " << std::endl;
  // 	  double delta = object.generate_delta();
  // 	  double temp = val*(1+eps_p*delta);
  // 	  //	  std::cout << val <<"\t" << temp << std::endl;
  // 	}
  //     }
  //   }
  // }

  // //Run gd

  // object.run_gd(maxiter);

  
	  
  
  
  
  // std::cout << "First pass " << std::endl;
  // std::cout << object.compute_gap_sgd() << std::endl;
  // std::cout << object.z << std::endl;
  // int mu_counter = 0; //counter for agent replacements
  // int p_counter = 0; //counter for price changes

  // int T = 400;
  // int counter = 0;

  // counter += 1;

  // int j=1;

  // while (j <T){
   
  //   if (j%50 == 0 && j >0){
  //     counter +=1;
  //     std::cout << "J " << j << " count " << counter << std::endl;
  //   }
  //   j+=1;
  // }

  // std::cout << "Final value of J " << j << std::endl;
  // if (j != T){
  //   counter +=1;
  //   std::cout << "Updating counter " << T << std::endl;
  // }
  // std::cout<< "Final count " << counter << std::endl;
  // //object.rr_procedure(2, 0);
  // //First pass to get analytical energy
  // // object.run_gd(maxiter);
  // // p_counter +=1;
  // // std::cout << object.z << "\t" << object.energy << std::endl;
  // // for (int i = 1; i<= T ; i++){
  // //   //replace constraints
  // //   object.replace_violated_constraints();
  // //   //update mu counter
  // //   mu_counter +=1;
  // //   //Run GD with new xi_matrix
  // //   object.run_gd(maxiter);
  // //   //Update p_counter
  // //   p_counter +=1;
  // //   std::cout << object.z << "\t" << object.energy << std::endl;
  // // }

  // // std::cout << "Mu replacements " << mu_counter << " prices counter " << p_counter << std::endl;
  
  
  
  // // int M = object.getM();
  // // object.run_sgd();

  // // double e1 = object.energy;

  // // object.replace_violated_constraints();

  // // object.run_sgd();

  // // double e2 = object.energy;

  // // // for (int i = 0; i < M; i++){
  // // //   Vector temp = test.xi_matrix.row(i);
  // // //   std::cout << (temp.transpose()*temp)/M<< std::endl;
  // // // }
  // // //std::cout << test.xi_matrix.sum()/(M*N) << std::endl;

  // // //std::cout << test.xi_matrix << std::endl;

  // // //std::cout << test.xi_matrix.row(0) << std::endl;
  // // // Matrix copy = test.xi_matrix;
  // // // std::cout << test.xi_matrix.transpose() << std::endl;
  // // // std::cout << " -------------------------------------- " << std::endl;

  // // // Matrix copy = normalize_cols(test);

  // // // std::cout << copy.transpose() << std::endl;
  // // // std::cout << " -------------------------------------- " << std::endl;
  // // // std::cout << test.xi_matrix.transpose() << std::endl;
  // // // std::cout << test.getM() << std::endl;

  // // Perceptron overlap(object.getalpha(),object.getN(),object.getsigma(), object.getDelta(), object.getpbar(), object.geteta());
  // // overlap.xi_matrix = object.xi_matrix;
  // // overlap.run_sgd();
  // // double q = 0.0;
  // // q = object.prices.transpose()*overlap.prices;
  // // q = q/N;
  // // std::cout << 1 << "\t" << q << "\t"  << overlap.z << "\t" << overlap.z*1.0/overlap.getM() << "\t"  << overlap.energy << "\t" << object.energy << std::endl;

  // // std::cout << "E1 " << object.energy << " E2 " << overlap.energy << " E3 " << e1 << " E4 " << e2 <<  std::endl;

  // // std::cout << "Computing the overlap differently" << std::endl;

  // // double qbis = 0.0;

  // // for (int i = 0; i < N; i++){
  // //   qbis = qbis + object.prices(i)*overlap.prices(i);
  // // }
  // // qbis = qbis/N;

  // // std::cout << "Qbis = " << qbis << std::endl
  //   ;
  
}
