#include "perceptron.hpp"

void open_file(std::ofstream &stream, const Perceptron& object){
  std::string underscore = std::string("_");
  std::string sigma_float = std::to_string(object.getsigma());


int main(int argc, char *argv[])
{

  // Create perceptron object
  double alpha = 2.0;
  int N = 10;
  double sigma = -0.5;
  double Delta = 4.5;
  double pbar = 1.5;
  double eta = 0.0;
  
  
  Perceptron object(alpha, N, sigma, Delta, pbar, eta);

  // std::cout << "Alpha : " << alpha << std::endl;

  // Vector pi_ema;
  // Vector pi_mu;
  // Vector viocons_temp;

  // int M = object.getM();
  // pi_ema.setZero(M);
  // pi_mu.setZero(M);
  // viocons_temp.setZero(M);
  // double energy = 0.0;

  // Vector sigma_vec = Vector::Constant(M, sigma);


  // Matrix xi_bar = object.perform_transactions();

  // for (int i = 0; i < object.getM(); i++){
  //   std::cout << " - - - - - - - - - - - "  << std::endl;
  //   std::cout << object.xi_matrix.row(i) << std::endl;
  //   std::cout << xi_bar.row(i) << std::endl;
  // }

  // Must create two different functions for soft and hard R&R

  /* Tests for new ema procedure 

     1. We initialize the xi_matrix and the prices
     2. We perform the transactions (xi_bar is updated)
     3. We initialize the hard/soft init rr 
     Check: sum of viocons_ema == 0 (for soft)
     Check sum of viocons_ema == sum of viocons (for hard)
     4. we recreate new prices 
     5. Perform transactions 
     6. run the hard/soft ema rr
     7. check sum of viocons_ema
  */

  // std::cout << "Tests for soft R&R with EMA " << std::endl;
  // double omega = 0.2;
  // std::cout << " Initializing prices " << std::endl;
  // std::cout << " -------------------------------------- " << std::endl;
  // object.init_prices(); // Initialize prices
  // std::cout << " Perform transactions " << std::endl;
  // std::cout << " -------------------------------------- " << std::endl;
  // object.perform_transactions();  // Perform transactions
  // std::cout << " Comparing xi_bar and xi_matrix " << std::endl;
  // std::cout << " -------------------------------------- " << std::endl;
  // for (int i=0; i <  object.getM(); i++){
  //   std::cout << object.xi_bar.row(i) << std::endl;
  //   std::cout << object.xi_matrix.row(i) << std::endl;
  //   std::cout << " -------------------------------------- " << std::endl;
  // }
  // std::cout << "Init hard ema " << std::endl;
  // std::cout << " -------------------------------------- " << std::endl;
  // object.init_soft_rr(); // Initialize rr
  // std::cout << " Initial viocons_ema " << std::endl;
  // std::cout << object.viocons_ema.sum() << std::endl;
  // std::cout << " -------------------------------------- " << std::endl;
  //  std::cout << " Comparing h_mu and pi_mu " << std::endl;
  // std::cout << " -------------------------------------- " << std::endl;
  // double energy = object.compute_gap_gd();
  // std::cout << object.pi_mu.transpose() << std::endl;
  // std::cout << object.h_mu.transpose() << std::endl;
  // for (int i = 0; i< 3; i++){
  //   std::cout << " -------------------------------------- " << std::endl;
  //   std::cout << " Initializing prices " << std::endl;
  //   std::cout << " -------------------------------------- " << std::endl;
  //   object.init_prices();  // Recreate prices
  //   std::cout << " Perform transactions " << std::endl;
  //   std::cout << " -------------------------------------- " << std::endl;
  //   object.perform_transactions(); // Perform transactions
  //   std::cout << " Compute h_mu" << std::endl;
  //   std::cout << " -------------------------------------- " << std::endl;
  //   energy = object.compute_gap_gd();
  //   std::cout << " Make ema Step" << std::endl;
  //   std::cout << " -------------------------------------- " << std::endl;
  //   object.soft_ema_rr(omega);
  //   std::cout << object.viocons_ema.transpose() << std::endl;
    // std::cout << object.viocons.transpose() << std::endl;
  }  



  // We compute the vectors h_mu here.
  //This populates the viocons as well and returns an energy as well
  // This is ideally not a good thing since I am doing two things within the same function.
  // The energy could ideally be calculated from the h_mu vectors. Atleast I shouldn't return it.
  
  // energy = object.compute_gap_gd();

  // actually no, we dont need the h_mu, we just need the dot-product between xi and prices


  // First pass

  // 1. Compute pi_mu 
  //pi_mu.noalias() = (1.0/sqrt(N))*object.xi_matrix*object.prices;
  // double energy_standard = object.compute_gap_gd();
  // // This computes the energy and the h_mu directly 
  // // 2. Collect the viocons and populate the pi_ema vector
  
  // for (int i = 0; i < M; ++i) {
  //   double temp = object.h_mu(i) - sigma;
  //   if(temp <  0.0){
  //     object.xi_matrix.row(i) = object.generate_random_vector(N, 0.0);
  //     viocons_temp(i) = 1;
  //     //energy += (temp * temp);
  //   }
  //   else{
  //     pi_ema(i) = pi_mu(i);
  //   }
  // }

  // //std::cout << "Viocons " << viocons_temp.sum() << " Energy " << energy_standard << std::endl;

  // // std::cout << "Viocons " << viocons_temp.sum() << " Z from perceptron " << object.z <<  " Energy " << (0.5/N)*energy << " Energy from perceptron " << energy_standard <<  std::endl;

  // // for (int i = 0; i < M; i++) {
  // //   std::cout << i << "\t" << viocons_temp(i) << "\t" << object.viocons(i) << "\t" << pi_mu(i) - sigma  << "\t" << object.h_mu(i) << "\t" << object.budgets(i) <<   std::endl;
  // // }
  // // std::cout << pi_ema.transpose() << std::endl;
  // // This is for the first step

  // // Once we have the first step done, we can now simulate the GD step

  
  // double omega = 0.2;

  // // 3. Generate new price vector to simulate the GD procedure
 
  // object.init_prices();
  // //4. Set energy to zero
  // //energy = 0.0;
  // //pi_mu.noalias() = (1.0/sqrt(N))*object.xi_matrix*object.prices;
  // std::cout << "Previous energy " << energy_standard << std::endl;
  // energy_standard = object.compute_gap_gd(); 

  // std::cout << "New energy after new prices " << energy_standard << std::endl;
  
  // for (int i = 0; i < M; i++){
  //   std::cout << i  << "\t" << viocons_temp(i) << std::endl;
  //   if (viocons_temp(i) == 0){
  //     pi_ema(i) = omega*object.h_mu(i) + (1-omega)*pi_ema(i);
  //     if (pi_ema(i) < sigma){
  // 	std::cout << "Viocon changed from 0 to 1 " << pi_ema(i) - sigma << std::endl;
  // 	viocons_temp(i) = 1;
  // 	std::cout << "Replacing agent" << std::endl;
  // 	object.xi_matrix.row(i) = object.generate_random_vector(N, 0.0);
  //     }
  //     else{
  // 	std::cout << "Viocon remains the same (0) " << pi_ema(i) - sigma << std::endl;
  //     }
  //   }

  //   else{
  //     pi_ema(i) = object.h_mu(i);
  //     if(pi_ema(i) < sigma){
  // 	std::cout << "Viocon remains the same (1) " << pi_ema(i) - sigma;
  // 	viocons_temp(i) = 1;
  // 	std::cout << "Replacing agent" << std::endl;
  // 	object.xi_matrix.row(i) = object.generate_random_vector(N, 0.0);
  //     }
  //     else{
  // 	std::cout << "viocon changes from 1 to 0 " << pi_ema(i) - sigma << std::endl;
  // 	viocons_temp(i) = 0;
  //     }
  //   }
  // }

  // std::cout << "Sum of viocons " << viocons_temp.sum() << std::endl;


  // for (int i =0; i < 10; i++){
  //   std::cout << object.generate_delta() << std::endl;
  // }

  // std::cout << object.xi_matrix.row(2) << std::endl;

  // object.aggregate_demand_supply();
  // object.update_demand_matrix();

  // std::cout<< object.xi_matrix.row(2) << std::endl;

  // // Must add code for testing the transaction step.
}
