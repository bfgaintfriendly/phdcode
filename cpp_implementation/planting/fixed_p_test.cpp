#include "../../perceptron.hpp"
//#include "../utils.hpp"



void planted_test(bool have_file){

  
}

  
int main(int argc, char *argv[]){

  //Create new perceptron object

  double alpha = 20.0;
  double eta = 0.0;
  double Delta = 4.5;

  int nb_runs = 1000;

  int maxiter = 20000;

  double sigma = std::atof(argv[1]);
  double pbar = std::atof(argv[2]);

  int N = 200;
  int T = 400;
  int copy = 1;
  
  Perceptron percep1(alpha, N, sigma, Delta, pbar, eta);
  percep1.init_xi();

  // // Run the gd procedure 
  // percep1.rr_procedure(T,copy);

  // // std::string underscore = std::string("_");

  std::string filename = "fixed_prices";

  //std::ofstream fixed(filename1);

  // assert(fixed.is_open());

  // fixed << percep1.prices.transpose() << std::endl;

  // fixed.close();

  // Vector fixed_prices = percep1.prices.transpose();

  //We now run the remove and replace procedure with fixed price vector;

  // Eigen::VectorXd fixed_prices(N);
  
  // std::cout << fixed_prices(0) << std::endl;
  // std::ifstream in (filename1, std::ios::in);

  // for (int i=0; i < N; i++){
  //   in >> fixed_prices(i);
  // }

  // std::cout << fixed_prices.sum()/N << std::endl;
  // std::cout << fixed_prices(0) << std::endl;
  // std::cout << "Rows in fixed prices " << fixed_prices.rows() << std::endl;
  // std::cout << fixed_prices.cols() << std::endl;
  // // std::string line;
  // // if (in.is_open()){
  // //   while (getline(in, line)){
  // //     std::cout << line << std::endl;
  // //     //fixed_prices >> line;
  // //   }
  // //   in.close();
  // // }
  // // else std::cout << "Unable to open file" << std::endl;

  // //std::cout << fixed_prices.sum() << std::endl;
  

  // // fixed_prices >> in

  // double sigma_RS = 0.5;
  // double sigma_RSB = -1.0;

  // Perceptron percep_RS(alpha, N, sigma_RS, Delta, pbar, eta);
  // std::cout << "Rows in perceptron prices " << percep_RS.prices.rows() << std::endl;
  

  // //Testing whether everything

  // std::cout << "Initial prices " << percep_RS.prices.sum()/N<< std::endl;
  // std::cout << "Fixed prices " << fixed_prices.sum() << std::endl;
  // percep_RS.prices = fixed_prices.transpose();
  // std::cout << "New prices " << percep_RS.prices.sum() << std::endl;


  // // We now run the replacement procedure;
  // std::cout << "Initial variables " << std::endl;
  // std::cout << "Energy " << percep_RS.compute_gap_gd() << std::endl;
  // std::cout << "z " << percep_RS.z << std::endl;
  // for (int i = 0; i < 2000; i++){
  //   percep_RS.replace_violated_constraints();
  //   double energy = percep_RS.compute_gap_gd();
  //   if (energy == 0){
  //     std::cout << "Energy reached zero" << std::endl;
  //     std::cout << "Energy " << percep_RS.compute_gap_gd() << " z " << percep_RS.z << std::endl;
  //     break;
  //   }
  //   std::cout << "Energy " << percep_RS.compute_gap_gd() << " z " << percep_RS.z << std::endl;
  // }

  
  percep1.planted_solution(filename, 1000);
}

